typedef long int ptrdiff_t;
typedef long unsigned int size_t;
typedef int wchar_t;
typedef struct {
  long long __max_align_ll __attribute__((__aligned__(__alignof__(long long))));
  long double __max_align_ld __attribute__((__aligned__(__alignof__(long double))));
} max_align_t;
typedef void * GC_PTR;
  typedef unsigned long GC_word;
  typedef long GC_signed_word;
extern unsigned GC_get_version(void);
extern GC_word GC_gc_no;
extern GC_word GC_get_gc_no(void);
typedef void * ( * GC_oom_func)(size_t );
extern GC_oom_func GC_oom_fn;
extern void GC_set_oom_fn(GC_oom_func);
extern GC_oom_func GC_get_oom_fn(void);
extern int GC_find_leak;
extern void GC_set_find_leak(int);
extern int GC_get_find_leak(void);
extern int GC_all_interior_pointers;
extern void GC_set_all_interior_pointers(int);
extern int GC_get_all_interior_pointers(void);
extern int GC_finalize_on_demand;
extern void GC_set_finalize_on_demand(int);
extern int GC_get_finalize_on_demand(void);
extern int GC_java_finalization;
extern void GC_set_java_finalization(int);
extern int GC_get_java_finalization(void);
typedef void ( * GC_finalizer_notifier_proc)(void);
extern GC_finalizer_notifier_proc GC_finalizer_notifier;
extern void GC_set_finalizer_notifier(GC_finalizer_notifier_proc);
extern GC_finalizer_notifier_proc GC_get_finalizer_notifier(void);
extern int GC_dont_gc;
extern int GC_dont_expand;
extern void GC_set_dont_expand(int);
extern int GC_get_dont_expand(void);
extern int GC_use_entire_heap;
extern int GC_full_freq;
extern void GC_set_full_freq(int);
extern int GC_get_full_freq(void);
extern GC_word GC_non_gc_bytes;
extern void GC_set_non_gc_bytes(GC_word);
extern GC_word GC_get_non_gc_bytes(void);
extern int GC_no_dls;
extern void GC_set_no_dls(int);
extern int GC_get_no_dls(void);
extern GC_word GC_free_space_divisor;
extern void GC_set_free_space_divisor(GC_word);
extern GC_word GC_get_free_space_divisor(void);
extern GC_word GC_max_retries;
extern void GC_set_max_retries(GC_word);
extern GC_word GC_get_max_retries(void);
extern char *GC_stackbottom;
extern int GC_dont_precollect;
extern void GC_set_dont_precollect(int);
extern int GC_get_dont_precollect(void);
extern unsigned long GC_time_limit;
extern void GC_set_time_limit(unsigned long);
extern unsigned long GC_get_time_limit(void);
extern void GC_set_pages_executable(int);
extern int GC_get_pages_executable(void);
extern void GC_set_handle_fork(int);
extern void GC_init(void);
extern void * GC_malloc(size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_malloc_atomic(size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern char * GC_strdup(const char *) __attribute__((__malloc__));
extern char * GC_strndup(const char *, size_t) __attribute__((__malloc__));
extern void * GC_malloc_uncollectable(size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_malloc_stubborn(size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_memalign(size_t , size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(2)));
extern int GC_posix_memalign(void ** , size_t ,
                        size_t );
extern void GC_free(void *);
extern void GC_change_stubborn(void *);
extern void GC_end_stubborn_change(void *);
extern void * GC_base(void * );
extern size_t GC_size(const void * );
extern void * GC_realloc(void * ,
                                 size_t )
                                             __attribute__((__alloc_size__(2)));
extern int GC_expand_hp(size_t );
extern void GC_set_max_heap_size(GC_word );
extern void GC_exclude_static_roots(void * ,
                                        void * );
extern void GC_clear_roots(void);
extern void GC_add_roots(void * ,
                                 void * );
extern void GC_remove_roots(void * ,
                                    void * );
extern void GC_register_displacement(size_t );
extern void GC_debug_register_displacement(size_t );
extern void GC_gcollect(void);
extern void GC_gcollect_and_unmap(void);
typedef int ( * GC_stop_func)(void);
extern int GC_try_to_collect(GC_stop_func );
extern void GC_set_stop_func(GC_stop_func );
extern GC_stop_func GC_get_stop_func(void);
extern size_t GC_get_heap_size(void);
extern size_t GC_get_free_bytes(void);
extern size_t GC_get_unmapped_bytes(void);
extern size_t GC_get_bytes_since_gc(void);
extern size_t GC_get_total_bytes(void);
extern void GC_get_heap_usage_safe(GC_word * ,
                                           GC_word * ,
                                           GC_word * ,
                                           GC_word * ,
                                           GC_word * );
extern void GC_disable(void);
extern int GC_is_disabled(void);
extern void GC_enable(void);
extern void GC_enable_incremental(void);
extern int GC_incremental_protection_needs(void);
extern int GC_collect_a_little(void);
extern void * GC_malloc_ignore_off_page(size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_malloc_atomic_ignore_off_page(size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_malloc_atomic_uncollectable(
                                                size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_debug_malloc_atomic_uncollectable(size_t,
                                                           const char * s, int i)
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_debug_malloc(size_t ,
                                      const char * s, int i)
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_debug_malloc_atomic(size_t ,
                                             const char * s, int i)
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern char * GC_debug_strdup(const char *,
                                      const char * s, int i) __attribute__((__malloc__));
extern char * GC_debug_strndup(const char *, size_t,
                                       const char * s, int i) __attribute__((__malloc__));
extern void * GC_debug_malloc_uncollectable(
                        size_t , const char * s, int i)
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_debug_malloc_stubborn(size_t ,
                                               const char * s, int i)
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_debug_malloc_ignore_off_page(
                        size_t , const char * s, int i)
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_debug_malloc_atomic_ignore_off_page(
                        size_t , const char * s, int i)
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void GC_debug_free(void *);
extern void * GC_debug_realloc(void * ,
                        size_t , const char * s, int i)
                                             __attribute__((__alloc_size__(2)));
extern void GC_debug_change_stubborn(void *);
extern void GC_debug_end_stubborn_change(void *);
extern void * GC_debug_malloc_replacement(size_t )
                        __attribute__((__malloc__)) __attribute__((__alloc_size__(1)));
extern void * GC_debug_realloc_replacement(void * ,
                                                   size_t )
                                             __attribute__((__alloc_size__(2)));
typedef void ( * GC_finalization_proc)(void * ,
                                                  void * );
extern void GC_register_finalizer(void * ,
                        GC_finalization_proc , void * ,
                        GC_finalization_proc * , void ** );
extern void GC_debug_register_finalizer(void * ,
                        GC_finalization_proc , void * ,
                        GC_finalization_proc * , void ** );
extern void GC_register_finalizer_ignore_self(void * ,
                        GC_finalization_proc , void * ,
                        GC_finalization_proc * , void ** );
extern void GC_debug_register_finalizer_ignore_self(void * ,
                        GC_finalization_proc , void * ,
                        GC_finalization_proc * , void ** );
extern void GC_register_finalizer_no_order(void * ,
                        GC_finalization_proc , void * ,
                        GC_finalization_proc * , void ** );
extern void GC_debug_register_finalizer_no_order(void * ,
                        GC_finalization_proc , void * ,
                        GC_finalization_proc * , void ** );
extern void GC_register_finalizer_unreachable(void * ,
                        GC_finalization_proc , void * ,
                        GC_finalization_proc * , void ** );
extern void GC_debug_register_finalizer_unreachable(void * ,
                        GC_finalization_proc , void * ,
                        GC_finalization_proc * , void ** );
extern int GC_register_disappearing_link(void ** );
extern int GC_general_register_disappearing_link(void ** ,
                                                         void * );
extern int GC_unregister_disappearing_link(void ** );
extern int GC_should_invoke_finalizers(void);
extern int GC_invoke_finalizers(void);
typedef void ( * GC_warn_proc)(char * ,
                                          GC_word );
extern void GC_set_warn_proc(GC_warn_proc );
extern GC_warn_proc GC_get_warn_proc(void);
extern void GC_ignore_warn_proc(char *, GC_word);
typedef GC_word GC_hidden_pointer;
typedef void * ( * GC_fn_type)(void * );
extern void * GC_call_with_alloc_lock(GC_fn_type ,
                                                void * );
struct GC_stack_base {
  void * mem_base;
};
typedef void * ( * GC_stack_base_func)(
                struct GC_stack_base * , void * );
extern void * GC_call_with_stack_base(GC_stack_base_func ,
                                              void * );
extern void * GC_do_blocking(GC_fn_type ,
                                     void * );
extern void * GC_call_with_gc_active(GC_fn_type ,
                                             void * );
extern int GC_get_stack_base(struct GC_stack_base *);
extern void * GC_same_obj(void * , void * );
extern void * GC_pre_incr(void **, ptrdiff_t );
extern void * GC_post_incr(void **, ptrdiff_t );
extern void * GC_is_visible(void * );
extern void * GC_is_valid_displacement(void * );
extern void GC_dump(void);
extern void ( * GC_same_obj_print_proc)(void * ,
                                                   void * );
extern void ( * GC_is_valid_displacement_print_proc)(void *);
extern void ( * GC_is_visible_print_proc)(void *);
extern void * GC_malloc_many(size_t );
typedef int ( * GC_has_static_roots_func)(
                                        const char * ,
                                        void * ,
                                        size_t );
extern void GC_register_has_static_roots_callback(
                                        GC_has_static_roots_func);
extern void GC_set_force_unmap_on_gcollect(int);
extern int GC_get_force_unmap_on_gcollect(void);
extern void GC_win32_free_heap(void);

extern void __assert_fail (const char *__assertion, const char *__file,
      unsigned int __line, const char *__function)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void __assert_perror_fail (int __errnum, const char *__file,
      unsigned int __line, const char *__function)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void __assert (const char *__assertion, const char *__file, int __line)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));


typedef unsigned char __u_char;
typedef unsigned short int __u_short;
typedef unsigned int __u_int;
typedef unsigned long int __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef signed short int __int16_t;
typedef unsigned short int __uint16_t;
typedef signed int __int32_t;
typedef unsigned int __uint32_t;
typedef signed long int __int64_t;
typedef unsigned long int __uint64_t;
typedef long int __quad_t;
typedef unsigned long int __u_quad_t;
typedef unsigned long int __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long int __ino_t;
typedef unsigned long int __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long int __nlink_t;
typedef long int __off_t;
typedef long int __off64_t;
typedef int __pid_t;
typedef struct { int __val[2]; } __fsid_t;
typedef long int __clock_t;
typedef unsigned long int __rlim_t;
typedef unsigned long int __rlim64_t;
typedef unsigned int __id_t;
typedef long int __time_t;
typedef unsigned int __useconds_t;
typedef long int __suseconds_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void * __timer_t;
typedef long int __blksize_t;
typedef long int __blkcnt_t;
typedef long int __blkcnt64_t;
typedef unsigned long int __fsblkcnt_t;
typedef unsigned long int __fsblkcnt64_t;
typedef unsigned long int __fsfilcnt_t;
typedef unsigned long int __fsfilcnt64_t;
typedef long int __fsword_t;
typedef long int __ssize_t;
typedef long int __syscall_slong_t;
typedef unsigned long int __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef __quad_t *__qaddr_t;
typedef char *__caddr_t;
typedef long int __intptr_t;
typedef unsigned int __socklen_t;
typedef __u_char u_char;
typedef __u_short u_short;
typedef __u_int u_int;
typedef __u_long u_long;
typedef __quad_t quad_t;
typedef __u_quad_t u_quad_t;
typedef __fsid_t fsid_t;
typedef __loff_t loff_t;
typedef __ino_t ino_t;
typedef __dev_t dev_t;
typedef __gid_t gid_t;
typedef __mode_t mode_t;
typedef __nlink_t nlink_t;
typedef __uid_t uid_t;
typedef __off_t off_t;
typedef __pid_t pid_t;
typedef __id_t id_t;
typedef __ssize_t ssize_t;
typedef __daddr_t daddr_t;
typedef __caddr_t caddr_t;
typedef __key_t key_t;

typedef __clock_t clock_t;



typedef __time_t time_t;


typedef __clockid_t clockid_t;
typedef __timer_t timer_t;
typedef unsigned long int ulong;
typedef unsigned short int ushort;
typedef unsigned int uint;
typedef int int8_t __attribute__ ((__mode__ (__QI__)));
typedef int int16_t __attribute__ ((__mode__ (__HI__)));
typedef int int32_t __attribute__ ((__mode__ (__SI__)));
typedef int int64_t __attribute__ ((__mode__ (__DI__)));
typedef unsigned int u_int8_t __attribute__ ((__mode__ (__QI__)));
typedef unsigned int u_int16_t __attribute__ ((__mode__ (__HI__)));
typedef unsigned int u_int32_t __attribute__ ((__mode__ (__SI__)));
typedef unsigned int u_int64_t __attribute__ ((__mode__ (__DI__)));
typedef int register_t __attribute__ ((__mode__ (__word__)));
static __inline unsigned int
__bswap_32 (unsigned int __bsx)
{
  return __builtin_bswap32 (__bsx);
}
static __inline __uint64_t
__bswap_64 (__uint64_t __bsx)
{
  return __builtin_bswap64 (__bsx);
}
typedef int __sig_atomic_t;
typedef struct
  {
    unsigned long int __val[(1024 / (8 * sizeof (unsigned long int)))];
  } __sigset_t;
typedef __sigset_t sigset_t;
struct timespec
  {
    __time_t tv_sec;
    __syscall_slong_t tv_nsec;
  };
struct timeval
  {
    __time_t tv_sec;
    __suseconds_t tv_usec;
  };
typedef __suseconds_t suseconds_t;
typedef long int __fd_mask;
typedef struct
  {
    __fd_mask __fds_bits[1024 / (8 * (int) sizeof (__fd_mask))];
  } fd_set;
typedef __fd_mask fd_mask;

extern int select (int __nfds, fd_set *__restrict __readfds,
     fd_set *__restrict __writefds,
     fd_set *__restrict __exceptfds,
     struct timeval *__restrict __timeout);
extern int pselect (int __nfds, fd_set *__restrict __readfds,
      fd_set *__restrict __writefds,
      fd_set *__restrict __exceptfds,
      const struct timespec *__restrict __timeout,
      const __sigset_t *__restrict __sigmask);


__extension__
extern unsigned int gnu_dev_major (unsigned long long int __dev)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
__extension__
extern unsigned int gnu_dev_minor (unsigned long long int __dev)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
__extension__
extern unsigned long long int gnu_dev_makedev (unsigned int __major,
            unsigned int __minor)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));

typedef __blksize_t blksize_t;
typedef __blkcnt_t blkcnt_t;
typedef __fsblkcnt_t fsblkcnt_t;
typedef __fsfilcnt_t fsfilcnt_t;
typedef unsigned long int pthread_t;
union pthread_attr_t
{
  char __size[56];
  long int __align;
};
typedef union pthread_attr_t pthread_attr_t;
typedef struct __pthread_internal_list
{
  struct __pthread_internal_list *__prev;
  struct __pthread_internal_list *__next;
} __pthread_list_t;
typedef union
{
  struct __pthread_mutex_s
  {
    int __lock;
    unsigned int __count;
    int __owner;
    unsigned int __nusers;
    int __kind;
    short __spins;
    short __elision;
    __pthread_list_t __list;
  } __data;
  char __size[40];
  long int __align;
} pthread_mutex_t;
typedef union
{
  char __size[4];
  int __align;
} pthread_mutexattr_t;
typedef union
{
  struct
  {
    int __lock;
    unsigned int __futex;
    __extension__ unsigned long long int __total_seq;
    __extension__ unsigned long long int __wakeup_seq;
    __extension__ unsigned long long int __woken_seq;
    void *__mutex;
    unsigned int __nwaiters;
    unsigned int __broadcast_seq;
  } __data;
  char __size[48];
  __extension__ long long int __align;
} pthread_cond_t;
typedef union
{
  char __size[4];
  int __align;
} pthread_condattr_t;
typedef unsigned int pthread_key_t;
typedef int pthread_once_t;
typedef union
{
  struct
  {
    int __lock;
    unsigned int __nr_readers;
    unsigned int __readers_wakeup;
    unsigned int __writer_wakeup;
    unsigned int __nr_readers_queued;
    unsigned int __nr_writers_queued;
    int __writer;
    int __shared;
    unsigned long int __pad1;
    unsigned long int __pad2;
    unsigned int __flags;
  } __data;
  char __size[56];
  long int __align;
} pthread_rwlock_t;
typedef union
{
  char __size[8];
  long int __align;
} pthread_rwlockattr_t;
typedef volatile int pthread_spinlock_t;
typedef union
{
  char __size[32];
  long int __align;
} pthread_barrier_t;
typedef union
{
  char __size[4];
  int __align;
} pthread_barrierattr_t;


typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long int uint64_t;
typedef signed char int_least8_t;
typedef short int int_least16_t;
typedef int int_least32_t;
typedef long int int_least64_t;
typedef unsigned char uint_least8_t;
typedef unsigned short int uint_least16_t;
typedef unsigned int uint_least32_t;
typedef unsigned long int uint_least64_t;
typedef signed char int_fast8_t;
typedef long int int_fast16_t;
typedef long int int_fast32_t;
typedef long int int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long int uint_fast16_t;
typedef unsigned long int uint_fast32_t;
typedef unsigned long int uint_fast64_t;
typedef long int intptr_t;
typedef unsigned long int uintptr_t;
typedef long int intmax_t;
typedef unsigned long int uintmax_t;
typedef uint16_t Elf32_Half;
typedef uint16_t Elf64_Half;
typedef uint32_t Elf32_Word;
typedef int32_t Elf32_Sword;
typedef uint32_t Elf64_Word;
typedef int32_t Elf64_Sword;
typedef uint64_t Elf32_Xword;
typedef int64_t Elf32_Sxword;
typedef uint64_t Elf64_Xword;
typedef int64_t Elf64_Sxword;
typedef uint32_t Elf32_Addr;
typedef uint64_t Elf64_Addr;
typedef uint32_t Elf32_Off;
typedef uint64_t Elf64_Off;
typedef uint16_t Elf32_Section;
typedef uint16_t Elf64_Section;
typedef Elf32_Half Elf32_Versym;
typedef Elf64_Half Elf64_Versym;
typedef struct
{
  unsigned char e_ident[(16)];
  Elf32_Half e_type;
  Elf32_Half e_machine;
  Elf32_Word e_version;
  Elf32_Addr e_entry;
  Elf32_Off e_phoff;
  Elf32_Off e_shoff;
  Elf32_Word e_flags;
  Elf32_Half e_ehsize;
  Elf32_Half e_phentsize;
  Elf32_Half e_phnum;
  Elf32_Half e_shentsize;
  Elf32_Half e_shnum;
  Elf32_Half e_shstrndx;
} Elf32_Ehdr;
typedef struct
{
  unsigned char e_ident[(16)];
  Elf64_Half e_type;
  Elf64_Half e_machine;
  Elf64_Word e_version;
  Elf64_Addr e_entry;
  Elf64_Off e_phoff;
  Elf64_Off e_shoff;
  Elf64_Word e_flags;
  Elf64_Half e_ehsize;
  Elf64_Half e_phentsize;
  Elf64_Half e_phnum;
  Elf64_Half e_shentsize;
  Elf64_Half e_shnum;
  Elf64_Half e_shstrndx;
} Elf64_Ehdr;
typedef struct
{
  Elf32_Word sh_name;
  Elf32_Word sh_type;
  Elf32_Word sh_flags;
  Elf32_Addr sh_addr;
  Elf32_Off sh_offset;
  Elf32_Word sh_size;
  Elf32_Word sh_link;
  Elf32_Word sh_info;
  Elf32_Word sh_addralign;
  Elf32_Word sh_entsize;
} Elf32_Shdr;
typedef struct
{
  Elf64_Word sh_name;
  Elf64_Word sh_type;
  Elf64_Xword sh_flags;
  Elf64_Addr sh_addr;
  Elf64_Off sh_offset;
  Elf64_Xword sh_size;
  Elf64_Word sh_link;
  Elf64_Word sh_info;
  Elf64_Xword sh_addralign;
  Elf64_Xword sh_entsize;
} Elf64_Shdr;
typedef struct
{
  Elf32_Word st_name;
  Elf32_Addr st_value;
  Elf32_Word st_size;
  unsigned char st_info;
  unsigned char st_other;
  Elf32_Section st_shndx;
} Elf32_Sym;
typedef struct
{
  Elf64_Word st_name;
  unsigned char st_info;
  unsigned char st_other;
  Elf64_Section st_shndx;
  Elf64_Addr st_value;
  Elf64_Xword st_size;
} Elf64_Sym;
typedef struct
{
  Elf32_Half si_boundto;
  Elf32_Half si_flags;
} Elf32_Syminfo;
typedef struct
{
  Elf64_Half si_boundto;
  Elf64_Half si_flags;
} Elf64_Syminfo;
typedef struct
{
  Elf32_Addr r_offset;
  Elf32_Word r_info;
} Elf32_Rel;
typedef struct
{
  Elf64_Addr r_offset;
  Elf64_Xword r_info;
} Elf64_Rel;
typedef struct
{
  Elf32_Addr r_offset;
  Elf32_Word r_info;
  Elf32_Sword r_addend;
} Elf32_Rela;
typedef struct
{
  Elf64_Addr r_offset;
  Elf64_Xword r_info;
  Elf64_Sxword r_addend;
} Elf64_Rela;
typedef struct
{
  Elf32_Word p_type;
  Elf32_Off p_offset;
  Elf32_Addr p_vaddr;
  Elf32_Addr p_paddr;
  Elf32_Word p_filesz;
  Elf32_Word p_memsz;
  Elf32_Word p_flags;
  Elf32_Word p_align;
} Elf32_Phdr;
typedef struct
{
  Elf64_Word p_type;
  Elf64_Word p_flags;
  Elf64_Off p_offset;
  Elf64_Addr p_vaddr;
  Elf64_Addr p_paddr;
  Elf64_Xword p_filesz;
  Elf64_Xword p_memsz;
  Elf64_Xword p_align;
} Elf64_Phdr;
typedef struct
{
  Elf32_Sword d_tag;
  union
    {
      Elf32_Word d_val;
      Elf32_Addr d_ptr;
    } d_un;
} Elf32_Dyn;
typedef struct
{
  Elf64_Sxword d_tag;
  union
    {
      Elf64_Xword d_val;
      Elf64_Addr d_ptr;
    } d_un;
} Elf64_Dyn;
typedef struct
{
  Elf32_Half vd_version;
  Elf32_Half vd_flags;
  Elf32_Half vd_ndx;
  Elf32_Half vd_cnt;
  Elf32_Word vd_hash;
  Elf32_Word vd_aux;
  Elf32_Word vd_next;
} Elf32_Verdef;
typedef struct
{
  Elf64_Half vd_version;
  Elf64_Half vd_flags;
  Elf64_Half vd_ndx;
  Elf64_Half vd_cnt;
  Elf64_Word vd_hash;
  Elf64_Word vd_aux;
  Elf64_Word vd_next;
} Elf64_Verdef;
typedef struct
{
  Elf32_Word vda_name;
  Elf32_Word vda_next;
} Elf32_Verdaux;
typedef struct
{
  Elf64_Word vda_name;
  Elf64_Word vda_next;
} Elf64_Verdaux;
typedef struct
{
  Elf32_Half vn_version;
  Elf32_Half vn_cnt;
  Elf32_Word vn_file;
  Elf32_Word vn_aux;
  Elf32_Word vn_next;
} Elf32_Verneed;
typedef struct
{
  Elf64_Half vn_version;
  Elf64_Half vn_cnt;
  Elf64_Word vn_file;
  Elf64_Word vn_aux;
  Elf64_Word vn_next;
} Elf64_Verneed;
typedef struct
{
  Elf32_Word vna_hash;
  Elf32_Half vna_flags;
  Elf32_Half vna_other;
  Elf32_Word vna_name;
  Elf32_Word vna_next;
} Elf32_Vernaux;
typedef struct
{
  Elf64_Word vna_hash;
  Elf64_Half vna_flags;
  Elf64_Half vna_other;
  Elf64_Word vna_name;
  Elf64_Word vna_next;
} Elf64_Vernaux;
typedef struct
{
  uint32_t a_type;
  union
    {
      uint32_t a_val;
    } a_un;
} Elf32_auxv_t;
typedef struct
{
  uint64_t a_type;
  union
    {
      uint64_t a_val;
    } a_un;
} Elf64_auxv_t;
typedef struct
{
  Elf32_Word n_namesz;
  Elf32_Word n_descsz;
  Elf32_Word n_type;
} Elf32_Nhdr;
typedef struct
{
  Elf64_Word n_namesz;
  Elf64_Word n_descsz;
  Elf64_Word n_type;
} Elf64_Nhdr;
typedef struct
{
  Elf32_Xword m_value;
  Elf32_Word m_info;
  Elf32_Word m_poffset;
  Elf32_Half m_repeat;
  Elf32_Half m_stride;
} Elf32_Move;
typedef struct
{
  Elf64_Xword m_value;
  Elf64_Xword m_info;
  Elf64_Xword m_poffset;
  Elf64_Half m_repeat;
  Elf64_Half m_stride;
} Elf64_Move;
typedef union
{
  struct
    {
      Elf32_Word gt_current_g_value;
      Elf32_Word gt_unused;
    } gt_header;
  struct
    {
      Elf32_Word gt_g_value;
      Elf32_Word gt_bytes;
    } gt_entry;
} Elf32_gptab;
typedef struct
{
  Elf32_Word ri_gprmask;
  Elf32_Word ri_cprmask[4];
  Elf32_Sword ri_gp_value;
} Elf32_RegInfo;
typedef struct
{
  unsigned char kind;
  unsigned char size;
  Elf32_Section section;
  Elf32_Word info;
} Elf_Options;
typedef struct
{
  Elf32_Word hwp_flags1;
  Elf32_Word hwp_flags2;
} Elf_Options_Hw;
typedef struct
{
  Elf32_Word l_name;
  Elf32_Word l_time_stamp;
  Elf32_Word l_checksum;
  Elf32_Word l_version;
  Elf32_Word l_flags;
} Elf32_Lib;
typedef struct
{
  Elf64_Word l_name;
  Elf64_Word l_time_stamp;
  Elf64_Word l_checksum;
  Elf64_Word l_version;
  Elf64_Word l_flags;
} Elf64_Lib;
typedef Elf32_Addr Elf32_Conflict;

typedef enum {
    ELF_C_NULL = 0,
    ELF_C_READ,
    ELF_C_WRITE,
    ELF_C_CLR,
    ELF_C_SET,
    ELF_C_FDDONE,
    ELF_C_FDREAD,
    ELF_C_RDWR,
    ELF_C_NUM
} Elf_Cmd;
typedef enum {
    ELF_K_NONE = 0,
    ELF_K_AR,
    ELF_K_COFF,
    ELF_K_ELF,
    ELF_K_NUM
} Elf_Kind;
typedef enum {
    ELF_T_BYTE = 0,
    ELF_T_ADDR,
    ELF_T_DYN,
    ELF_T_EHDR,
    ELF_T_HALF,
    ELF_T_OFF,
    ELF_T_PHDR,
    ELF_T_RELA,
    ELF_T_REL,
    ELF_T_SHDR,
    ELF_T_SWORD,
    ELF_T_SYM,
    ELF_T_WORD,
    ELF_T_SXWORD,
    ELF_T_XWORD,
    ELF_T_VDEF,
    ELF_T_VNEED,
    ELF_T_NUM
} Elf_Type;
typedef struct Elf Elf;
typedef struct Elf_Scn Elf_Scn;
typedef struct {
    char* ar_name;
    time_t ar_date;
    long ar_uid;
    long ar_gid;
    unsigned long ar_mode;
    off_t ar_size;
    char* ar_rawname;
} Elf_Arhdr;
typedef struct {
    char* as_name;
    size_t as_off;
    unsigned long as_hash;
} Elf_Arsym;
typedef struct {
    void* d_buf;
    Elf_Type d_type;
    size_t d_size;
    off_t d_off;
    size_t d_align;
    unsigned d_version;
} Elf_Data;
extern Elf *elf_begin (int __fd, Elf_Cmd __cmd, Elf *__ref);
extern Elf *elf_memory (char *__image, size_t __size);
extern int elf_cntl (Elf *__elf, Elf_Cmd __cmd);
extern int elf_end (Elf *__elf);
extern const char *elf_errmsg (int __err);
extern int elf_errno (void);
extern void elf_fill (int __fill);
extern unsigned elf_flagdata (Elf_Data *__data, Elf_Cmd __cmd, unsigned __flags);
extern unsigned elf_flagehdr (Elf *__elf, Elf_Cmd __cmd, unsigned __flags);
extern unsigned elf_flagelf (Elf *__elf, Elf_Cmd __cmd, unsigned __flags);
extern unsigned elf_flagphdr (Elf *__elf, Elf_Cmd __cmd, unsigned __flags);
extern unsigned elf_flagscn (Elf_Scn *__scn, Elf_Cmd __cmd, unsigned __flags);
extern unsigned elf_flagshdr (Elf_Scn *__scn, Elf_Cmd __cmd, unsigned __flags);
extern size_t elf32_fsize (Elf_Type __type, size_t __count, unsigned __ver);
extern Elf_Arhdr *elf_getarhdr (Elf *__elf);
extern Elf_Arsym *elf_getarsym (Elf *__elf, size_t *__ptr);
extern off_t elf_getbase (Elf *__elf);
extern Elf_Data *elf_getdata (Elf_Scn *__scn, Elf_Data *__data);
extern Elf32_Ehdr *elf32_getehdr (Elf *__elf);
extern char *elf_getident (Elf *__elf, size_t *__ptr);
extern Elf32_Phdr *elf32_getphdr (Elf *__elf);
extern Elf_Scn *elf_getscn (Elf *__elf, size_t __index);
extern Elf32_Shdr *elf32_getshdr (Elf_Scn *__scn);
extern unsigned long elf_hash (const unsigned char *__name);
extern Elf_Kind elf_kind (Elf *__elf);
extern size_t elf_ndxscn (Elf_Scn *__scn);
extern Elf_Data *elf_newdata (Elf_Scn *__scn);
extern Elf32_Ehdr *elf32_newehdr (Elf *__elf);
extern Elf32_Phdr *elf32_newphdr (Elf *__elf, size_t __count);
extern Elf_Scn *elf_newscn (Elf *__elf);
extern Elf_Cmd elf_next (Elf *__elf);
extern Elf_Scn *elf_nextscn (Elf *__elf, Elf_Scn *__scn);
extern size_t elf_rand (Elf *__elf, size_t __offset);
extern Elf_Data *elf_rawdata (Elf_Scn *__scn, Elf_Data *__data);
extern char *elf_rawfile (Elf *__elf, size_t *__ptr);
extern char *elf_strptr (Elf *__elf, size_t __section, size_t __offset);
extern off_t elf_update (Elf *__elf, Elf_Cmd __cmd);
extern unsigned elf_version (unsigned __ver);
extern Elf_Data *elf32_xlatetof (Elf_Data *__dst, const Elf_Data *__src, unsigned __encode);
extern Elf_Data *elf32_xlatetom (Elf_Data *__dst, const Elf_Data *__src, unsigned __encode);
extern long elf32_checksum (Elf *__elf);
extern Elf64_Ehdr *elf64_getehdr (Elf *__elf);
extern Elf64_Ehdr *elf64_newehdr (Elf *__elf);
extern Elf64_Phdr *elf64_getphdr (Elf *__elf);
extern Elf64_Phdr *elf64_newphdr (Elf *__elf, size_t __count);
extern Elf64_Shdr *elf64_getshdr (Elf_Scn *__scn);
extern size_t elf64_fsize (Elf_Type __type, size_t __count, unsigned __ver);
extern Elf_Data *elf64_xlatetof (Elf_Data *__dst, const Elf_Data *__src, unsigned __encode);
extern Elf_Data *elf64_xlatetom (Elf_Data *__dst, const Elf_Data *__src, unsigned __encode);
extern long elf64_checksum (Elf *__elf);
__attribute__((deprecated)) extern int elf_getphnum (Elf *__elf, size_t *__resultp);
__attribute__((deprecated)) extern int elf_getshnum (Elf *__elf, size_t *__resultp);
__attribute__((deprecated)) extern int elf_getshstrndx (Elf *__elf, size_t *__resultp);
extern int elf_getphdrnum (Elf *__elf, size_t *__resultp);
extern int elf_getshdrnum (Elf *__elf, size_t *__resultp);
extern int elf_getshdrstrndx (Elf *__elf, size_t *__resultp);
extern int elfx_update_shstrndx (Elf *__elf, size_t __index);
extern size_t elfx_movscn (Elf *__elf, Elf_Scn *__scn, Elf_Scn *__after);
extern size_t elfx_remscn (Elf *__elf, Elf_Scn *__scn);
extern size_t elf_delscn (Elf *__elf, Elf_Scn *__scn);

struct _IO_FILE;

typedef struct _IO_FILE FILE;


typedef struct _IO_FILE __FILE;
typedef struct
{
  int __count;
  union
  {
    unsigned int __wch;
    char __wchb[4];
  } __value;
} __mbstate_t;
typedef struct
{
  __off_t __pos;
  __mbstate_t __state;
} _G_fpos_t;
typedef struct
{
  __off64_t __pos;
  __mbstate_t __state;
} _G_fpos64_t;
typedef __builtin_va_list __gnuc_va_list;
struct _IO_jump_t; struct _IO_FILE;
typedef void _IO_lock_t;
struct _IO_marker {
  struct _IO_marker *_next;
  struct _IO_FILE *_sbuf;
  int _pos;
};
enum __codecvt_result
{
  __codecvt_ok,
  __codecvt_partial,
  __codecvt_error,
  __codecvt_noconv
};
struct _IO_FILE {
  int _flags;
  char* _IO_read_ptr;
  char* _IO_read_end;
  char* _IO_read_base;
  char* _IO_write_base;
  char* _IO_write_ptr;
  char* _IO_write_end;
  char* _IO_buf_base;
  char* _IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[15 * sizeof (int) - 4 * sizeof (void *) - sizeof (size_t)];
};
typedef struct _IO_FILE _IO_FILE;
struct _IO_FILE_plus;
extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
typedef __ssize_t __io_read_fn (void *__cookie, char *__buf, size_t __nbytes);
typedef __ssize_t __io_write_fn (void *__cookie, const char *__buf,
     size_t __n);
typedef int __io_seek_fn (void *__cookie, __off64_t *__pos, int __w);
typedef int __io_close_fn (void *__cookie);
extern int __underflow (_IO_FILE *);
extern int __uflow (_IO_FILE *);
extern int __overflow (_IO_FILE *, int);
extern int _IO_getc (_IO_FILE *__fp);
extern int _IO_putc (int __c, _IO_FILE *__fp);
extern int _IO_feof (_IO_FILE *__fp) __attribute__ ((__nothrow__ , __leaf__));
extern int _IO_ferror (_IO_FILE *__fp) __attribute__ ((__nothrow__ , __leaf__));
extern int _IO_peekc_locked (_IO_FILE *__fp);
extern void _IO_flockfile (_IO_FILE *) __attribute__ ((__nothrow__ , __leaf__));
extern void _IO_funlockfile (_IO_FILE *) __attribute__ ((__nothrow__ , __leaf__));
extern int _IO_ftrylockfile (_IO_FILE *) __attribute__ ((__nothrow__ , __leaf__));
extern int _IO_vfscanf (_IO_FILE * __restrict, const char * __restrict,
   __gnuc_va_list, int *__restrict);
extern int _IO_vfprintf (_IO_FILE *__restrict, const char *__restrict,
    __gnuc_va_list);
extern __ssize_t _IO_padn (_IO_FILE *, int, __ssize_t);
extern size_t _IO_sgetn (_IO_FILE *, void *, size_t);
extern __off64_t _IO_seekoff (_IO_FILE *, __off64_t, int, int);
extern __off64_t _IO_seekpos (_IO_FILE *, __off64_t, int);
extern void _IO_free_backup_area (_IO_FILE *) __attribute__ ((__nothrow__ , __leaf__));
typedef __gnuc_va_list va_list;

typedef _G_fpos_t fpos_t;

extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;

extern int remove (const char *__filename) __attribute__ ((__nothrow__ , __leaf__));
extern int rename (const char *__old, const char *__new) __attribute__ ((__nothrow__ , __leaf__));

extern int renameat (int __oldfd, const char *__old, int __newfd,
       const char *__new) __attribute__ ((__nothrow__ , __leaf__));

extern FILE *tmpfile (void) ;
extern char *tmpnam (char *__s) __attribute__ ((__nothrow__ , __leaf__)) ;

extern char *tmpnam_r (char *__s) __attribute__ ((__nothrow__ , __leaf__)) ;
extern char *tempnam (const char *__dir, const char *__pfx)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;

extern int fclose (FILE *__stream);
extern int fflush (FILE *__stream);

extern int fflush_unlocked (FILE *__stream);

extern FILE *fopen (const char *__restrict __filename,
      const char *__restrict __modes) ;
extern FILE *freopen (const char *__restrict __filename,
        const char *__restrict __modes,
        FILE *__restrict __stream) ;

extern FILE *fdopen (int __fd, const char *__modes) __attribute__ ((__nothrow__ , __leaf__)) ;
extern FILE *fmemopen (void *__s, size_t __len, const char *__modes)
  __attribute__ ((__nothrow__ , __leaf__)) ;
extern FILE *open_memstream (char **__bufloc, size_t *__sizeloc) __attribute__ ((__nothrow__ , __leaf__)) ;

extern void setbuf (FILE *__restrict __stream, char *__restrict __buf) __attribute__ ((__nothrow__ , __leaf__));
extern int setvbuf (FILE *__restrict __stream, char *__restrict __buf,
      int __modes, size_t __n) __attribute__ ((__nothrow__ , __leaf__));

extern void setbuffer (FILE *__restrict __stream, char *__restrict __buf,
         size_t __size) __attribute__ ((__nothrow__ , __leaf__));
extern void setlinebuf (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));

extern int fprintf (FILE *__restrict __stream,
      const char *__restrict __format, ...);
extern int printf (const char *__restrict __format, ...);
extern int sprintf (char *__restrict __s,
      const char *__restrict __format, ...) __attribute__ ((__nothrow__));
extern int vfprintf (FILE *__restrict __s, const char *__restrict __format,
       __gnuc_va_list __arg);
extern int vprintf (const char *__restrict __format, __gnuc_va_list __arg);
extern int vsprintf (char *__restrict __s, const char *__restrict __format,
       __gnuc_va_list __arg) __attribute__ ((__nothrow__));


extern int snprintf (char *__restrict __s, size_t __maxlen,
       const char *__restrict __format, ...)
     __attribute__ ((__nothrow__)) __attribute__ ((__format__ (__printf__, 3, 4)));
extern int vsnprintf (char *__restrict __s, size_t __maxlen,
        const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__nothrow__)) __attribute__ ((__format__ (__printf__, 3, 0)));

extern int vdprintf (int __fd, const char *__restrict __fmt,
       __gnuc_va_list __arg)
     __attribute__ ((__format__ (__printf__, 2, 0)));
extern int dprintf (int __fd, const char *__restrict __fmt, ...)
     __attribute__ ((__format__ (__printf__, 2, 3)));

extern int fscanf (FILE *__restrict __stream,
     const char *__restrict __format, ...) ;
extern int scanf (const char *__restrict __format, ...) ;
extern int sscanf (const char *__restrict __s,
     const char *__restrict __format, ...) __attribute__ ((__nothrow__ , __leaf__));
extern int fscanf (FILE *__restrict __stream, const char *__restrict __format, ...) __asm__ ("" "__isoc99_fscanf") ;
extern int scanf (const char *__restrict __format, ...) __asm__ ("" "__isoc99_scanf") ;
extern int sscanf (const char *__restrict __s, const char *__restrict __format, ...) __asm__ ("" "__isoc99_sscanf") __attribute__ ((__nothrow__ , __leaf__));


extern int vfscanf (FILE *__restrict __s, const char *__restrict __format,
      __gnuc_va_list __arg)
     __attribute__ ((__format__ (__scanf__, 2, 0))) ;
extern int vscanf (const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__format__ (__scanf__, 1, 0))) ;
extern int vsscanf (const char *__restrict __s,
      const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__format__ (__scanf__, 2, 0)));
extern int vfscanf (FILE *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vfscanf")
     __attribute__ ((__format__ (__scanf__, 2, 0))) ;
extern int vscanf (const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vscanf")
     __attribute__ ((__format__ (__scanf__, 1, 0))) ;
extern int vsscanf (const char *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vsscanf") __attribute__ ((__nothrow__ , __leaf__))
     __attribute__ ((__format__ (__scanf__, 2, 0)));


extern int fgetc (FILE *__stream);
extern int getc (FILE *__stream);
extern int getchar (void);

extern int getc_unlocked (FILE *__stream);
extern int getchar_unlocked (void);
extern int fgetc_unlocked (FILE *__stream);

extern int fputc (int __c, FILE *__stream);
extern int putc (int __c, FILE *__stream);
extern int putchar (int __c);

extern int fputc_unlocked (int __c, FILE *__stream);
extern int putc_unlocked (int __c, FILE *__stream);
extern int putchar_unlocked (int __c);
extern int getw (FILE *__stream);
extern int putw (int __w, FILE *__stream);

extern char *fgets (char *__restrict __s, int __n, FILE *__restrict __stream)
     ;

extern __ssize_t __getdelim (char **__restrict __lineptr,
          size_t *__restrict __n, int __delimiter,
          FILE *__restrict __stream) ;
extern __ssize_t getdelim (char **__restrict __lineptr,
        size_t *__restrict __n, int __delimiter,
        FILE *__restrict __stream) ;
extern __ssize_t getline (char **__restrict __lineptr,
       size_t *__restrict __n,
       FILE *__restrict __stream) ;

extern int fputs (const char *__restrict __s, FILE *__restrict __stream);
extern int puts (const char *__s);
extern int ungetc (int __c, FILE *__stream);
extern size_t fread (void *__restrict __ptr, size_t __size,
       size_t __n, FILE *__restrict __stream) ;
extern size_t fwrite (const void *__restrict __ptr, size_t __size,
        size_t __n, FILE *__restrict __s);

extern size_t fread_unlocked (void *__restrict __ptr, size_t __size,
         size_t __n, FILE *__restrict __stream) ;
extern size_t fwrite_unlocked (const void *__restrict __ptr, size_t __size,
          size_t __n, FILE *__restrict __stream);

extern int fseek (FILE *__stream, long int __off, int __whence);
extern long int ftell (FILE *__stream) ;
extern void rewind (FILE *__stream);

extern int fseeko (FILE *__stream, __off_t __off, int __whence);
extern __off_t ftello (FILE *__stream) ;

extern int fgetpos (FILE *__restrict __stream, fpos_t *__restrict __pos);
extern int fsetpos (FILE *__stream, const fpos_t *__pos);


extern void clearerr (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));
extern int feof (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int ferror (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;

extern void clearerr_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));
extern int feof_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int ferror_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;

extern void perror (const char *__s);

extern int sys_nerr;
extern const char *const sys_errlist[];
extern int fileno (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int fileno_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
extern FILE *popen (const char *__command, const char *__modes) ;
extern int pclose (FILE *__stream);
extern char *ctermid (char *__s) __attribute__ ((__nothrow__ , __leaf__));
extern void flockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));
extern int ftrylockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
extern void funlockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));



extern void *memcpy (void *__restrict __dest, const void *__restrict __src,
       size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern void *memmove (void *__dest, const void *__src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern void *memccpy (void *__restrict __dest, const void *__restrict __src,
        int __c, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern void *memset (void *__s, int __c, size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int memcmp (const void *__s1, const void *__s2, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern void *memchr (const void *__s, int __c, size_t __n)
      __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));


extern char *strcpy (char *__restrict __dest, const char *__restrict __src)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *strncpy (char *__restrict __dest,
        const char *__restrict __src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *strcat (char *__restrict __dest, const char *__restrict __src)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *strncat (char *__restrict __dest, const char *__restrict __src,
        size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int strcmp (const char *__s1, const char *__s2)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern int strncmp (const char *__s1, const char *__s2, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern int strcoll (const char *__s1, const char *__s2)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern size_t strxfrm (char *__restrict __dest,
         const char *__restrict __src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));

typedef struct __locale_struct
{
  struct __locale_data *__locales[13];
  const unsigned short int *__ctype_b;
  const int *__ctype_tolower;
  const int *__ctype_toupper;
  const char *__names[13];
} *__locale_t;
typedef __locale_t locale_t;
extern int strcoll_l (const char *__s1, const char *__s2, __locale_t __l)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2, 3)));
extern size_t strxfrm_l (char *__dest, const char *__src, size_t __n,
    __locale_t __l) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 4)));
extern char *strdup (const char *__s)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) __attribute__ ((__nonnull__ (1)));
extern char *strndup (const char *__string, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) __attribute__ ((__nonnull__ (1)));

extern char *strchr (const char *__s, int __c)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));
extern char *strrchr (const char *__s, int __c)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));


extern size_t strcspn (const char *__s, const char *__reject)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern size_t strspn (const char *__s, const char *__accept)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *strpbrk (const char *__s, const char *__accept)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *strstr (const char *__haystack, const char *__needle)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *strtok (char *__restrict __s, const char *__restrict __delim)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));

extern char *__strtok_r (char *__restrict __s,
    const char *__restrict __delim,
    char **__restrict __save_ptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 3)));
extern char *strtok_r (char *__restrict __s, const char *__restrict __delim,
         char **__restrict __save_ptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 3)));

extern size_t strlen (const char *__s)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));

extern size_t strnlen (const char *__string, size_t __maxlen)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));

extern char *strerror (int __errnum) __attribute__ ((__nothrow__ , __leaf__));

extern int strerror_r (int __errnum, char *__buf, size_t __buflen) __asm__ ("" "__xpg_strerror_r") __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern char *strerror_l (int __errnum, __locale_t __l) __attribute__ ((__nothrow__ , __leaf__));
extern void __bzero (void *__s, size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern void bcopy (const void *__src, void *__dest, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern void bzero (void *__s, size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int bcmp (const void *__s1, const void *__s2, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *index (const char *__s, int __c)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));
extern char *rindex (const char *__s, int __c)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));
extern int ffs (int __i) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
extern int strcasecmp (const char *__s1, const char *__s2)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern int strncasecmp (const char *__s1, const char *__s2, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *strsep (char **__restrict __stringp,
       const char *__restrict __delim)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *strsignal (int __sig) __attribute__ ((__nothrow__ , __leaf__));
extern char *__stpcpy (char *__restrict __dest, const char *__restrict __src)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *stpcpy (char *__restrict __dest, const char *__restrict __src)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *__stpncpy (char *__restrict __dest,
   const char *__restrict __src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern char *stpncpy (char *__restrict __dest,
        const char *__restrict __src, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));


typedef enum
{
  P_ALL,
  P_PID,
  P_PGID
} idtype_t;
union wait
  {
    int w_status;
    struct
      {
 unsigned int __w_termsig:7;
 unsigned int __w_coredump:1;
 unsigned int __w_retcode:8;
 unsigned int:16;
      } __wait_terminated;
    struct
      {
 unsigned int __w_stopval:8;
 unsigned int __w_stopsig:8;
 unsigned int:16;
      } __wait_stopped;
  };
typedef union
  {
    union wait *__uptr;
    int *__iptr;
  } __WAIT_STATUS __attribute__ ((__transparent_union__));

typedef struct
  {
    int quot;
    int rem;
  } div_t;
typedef struct
  {
    long int quot;
    long int rem;
  } ldiv_t;


__extension__ typedef struct
  {
    long long int quot;
    long long int rem;
  } lldiv_t;

extern size_t __ctype_get_mb_cur_max (void) __attribute__ ((__nothrow__ , __leaf__)) ;

extern double atof (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;
extern int atoi (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;
extern long int atol (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;


__extension__ extern long long int atoll (const char *__nptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;


extern double strtod (const char *__restrict __nptr,
        char **__restrict __endptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern float strtof (const char *__restrict __nptr,
       char **__restrict __endptr) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern long double strtold (const char *__restrict __nptr,
       char **__restrict __endptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


extern long int strtol (const char *__restrict __nptr,
   char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern unsigned long int strtoul (const char *__restrict __nptr,
      char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

__extension__
extern long long int strtoq (const char *__restrict __nptr,
        char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
__extension__
extern unsigned long long int strtouq (const char *__restrict __nptr,
           char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

__extension__
extern long long int strtoll (const char *__restrict __nptr,
         char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
__extension__
extern unsigned long long int strtoull (const char *__restrict __nptr,
     char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

extern char *l64a (long int __n) __attribute__ ((__nothrow__ , __leaf__)) ;
extern long int a64l (const char *__s)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1))) ;
extern long int random (void) __attribute__ ((__nothrow__ , __leaf__));
extern void srandom (unsigned int __seed) __attribute__ ((__nothrow__ , __leaf__));
extern char *initstate (unsigned int __seed, char *__statebuf,
   size_t __statelen) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern char *setstate (char *__statebuf) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
struct random_data
  {
    int32_t *fptr;
    int32_t *rptr;
    int32_t *state;
    int rand_type;
    int rand_deg;
    int rand_sep;
    int32_t *end_ptr;
  };
extern int random_r (struct random_data *__restrict __buf,
       int32_t *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int srandom_r (unsigned int __seed, struct random_data *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int initstate_r (unsigned int __seed, char *__restrict __statebuf,
   size_t __statelen,
   struct random_data *__restrict __buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 4)));
extern int setstate_r (char *__restrict __statebuf,
         struct random_data *__restrict __buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern int rand (void) __attribute__ ((__nothrow__ , __leaf__));
extern void srand (unsigned int __seed) __attribute__ ((__nothrow__ , __leaf__));

extern int rand_r (unsigned int *__seed) __attribute__ ((__nothrow__ , __leaf__));
extern double drand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern double erand48 (unsigned short int __xsubi[3]) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern long int lrand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern long int nrand48 (unsigned short int __xsubi[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern long int mrand48 (void) __attribute__ ((__nothrow__ , __leaf__));
extern long int jrand48 (unsigned short int __xsubi[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern void srand48 (long int __seedval) __attribute__ ((__nothrow__ , __leaf__));
extern unsigned short int *seed48 (unsigned short int __seed16v[3])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern void lcong48 (unsigned short int __param[7]) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
struct drand48_data
  {
    unsigned short int __x[3];
    unsigned short int __old_x[3];
    unsigned short int __c;
    unsigned short int __init;
    __extension__ unsigned long long int __a;
  };
extern int drand48_r (struct drand48_data *__restrict __buffer,
        double *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int erand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        double *__restrict __result) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int lrand48_r (struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int nrand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int mrand48_r (struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int jrand48_r (unsigned short int __xsubi[3],
        struct drand48_data *__restrict __buffer,
        long int *__restrict __result)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int srand48_r (long int __seedval, struct drand48_data *__buffer)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int seed48_r (unsigned short int __seed16v[3],
       struct drand48_data *__buffer) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int lcong48_r (unsigned short int __param[7],
        struct drand48_data *__buffer)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));

extern void *malloc (size_t __size) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;
extern void *calloc (size_t __nmemb, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;


extern void *realloc (void *__ptr, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__warn_unused_result__));
extern void free (void *__ptr) __attribute__ ((__nothrow__ , __leaf__));

extern void cfree (void *__ptr) __attribute__ ((__nothrow__ , __leaf__));

extern void *alloca (size_t __size) __attribute__ ((__nothrow__ , __leaf__));

extern void *valloc (size_t __size) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;
extern int posix_memalign (void **__memptr, size_t __alignment, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern void *aligned_alloc (size_t __alignment, size_t __size)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) __attribute__ ((__alloc_size__ (2))) ;

extern void abort (void) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern int atexit (void (*__func) (void)) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int at_quick_exit (void (*__func) (void)) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

extern int on_exit (void (*__func) (int __status, void *__arg), void *__arg)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));

extern void exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
extern void quick_exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));


extern void _Exit (int __status) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));


extern char *getenv (const char *__name) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;

extern int putenv (char *__string) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int setenv (const char *__name, const char *__value, int __replace)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int unsetenv (const char *__name) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int clearenv (void) __attribute__ ((__nothrow__ , __leaf__));
extern char *mktemp (char *__template) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int mkstemp (char *__template) __attribute__ ((__nonnull__ (1))) ;
extern int mkstemps (char *__template, int __suffixlen) __attribute__ ((__nonnull__ (1))) ;
extern char *mkdtemp (char *__template) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;

extern int system (const char *__command) ;

extern char *realpath (const char *__restrict __name,
         char *__restrict __resolved) __attribute__ ((__nothrow__ , __leaf__)) ;
typedef int (*__compar_fn_t) (const void *, const void *);

extern void *bsearch (const void *__key, const void *__base,
        size_t __nmemb, size_t __size, __compar_fn_t __compar)
     __attribute__ ((__nonnull__ (1, 2, 5))) ;
extern void qsort (void *__base, size_t __nmemb, size_t __size,
     __compar_fn_t __compar) __attribute__ ((__nonnull__ (1, 4)));
extern int abs (int __x) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;
extern long int labs (long int __x) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;

__extension__ extern long long int llabs (long long int __x)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;

extern div_t div (int __numer, int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;
extern ldiv_t ldiv (long int __numer, long int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;


__extension__ extern lldiv_t lldiv (long long int __numer,
        long long int __denom)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__)) ;

extern char *ecvt (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) ;
extern char *fcvt (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) ;
extern char *gcvt (double __value, int __ndigit, char *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3))) ;
extern char *qecvt (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) ;
extern char *qfcvt (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4))) ;
extern char *qgcvt (long double __value, int __ndigit, char *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3))) ;
extern int ecvt_r (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign, char *__restrict __buf,
     size_t __len) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));
extern int fcvt_r (double __value, int __ndigit, int *__restrict __decpt,
     int *__restrict __sign, char *__restrict __buf,
     size_t __len) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));
extern int qecvt_r (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign,
      char *__restrict __buf, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));
extern int qfcvt_r (long double __value, int __ndigit,
      int *__restrict __decpt, int *__restrict __sign,
      char *__restrict __buf, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4, 5)));

extern int mblen (const char *__s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));
extern int mbtowc (wchar_t *__restrict __pwc,
     const char *__restrict __s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));
extern int wctomb (char *__s, wchar_t __wchar) __attribute__ ((__nothrow__ , __leaf__));
extern size_t mbstowcs (wchar_t *__restrict __pwcs,
   const char *__restrict __s, size_t __n) __attribute__ ((__nothrow__ , __leaf__));
extern size_t wcstombs (char *__restrict __s,
   const wchar_t *__restrict __pwcs, size_t __n)
     __attribute__ ((__nothrow__ , __leaf__));

extern int rpmatch (const char *__response) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int getsubopt (char **__restrict __optionp,
        char *const *__restrict __tokens,
        char **__restrict __valuep)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2, 3))) ;
extern int getloadavg (double __loadavg[], int __nelem)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));


struct winsize
  {
    unsigned short int ws_row;
    unsigned short int ws_col;
    unsigned short int ws_xpixel;
    unsigned short int ws_ypixel;
  };
struct termio
  {
    unsigned short int c_iflag;
    unsigned short int c_oflag;
    unsigned short int c_cflag;
    unsigned short int c_lflag;
    unsigned char c_line;
    unsigned char c_cc[8];
};
extern int ioctl (int __fd, unsigned long int __request, ...) __attribute__ ((__nothrow__ , __leaf__));


extern void *mmap (void *__addr, size_t __len, int __prot,
     int __flags, int __fd, __off_t __offset) __attribute__ ((__nothrow__ , __leaf__));
extern int munmap (void *__addr, size_t __len) __attribute__ ((__nothrow__ , __leaf__));
extern int mprotect (void *__addr, size_t __len, int __prot) __attribute__ ((__nothrow__ , __leaf__));
extern int msync (void *__addr, size_t __len, int __flags);
extern int madvise (void *__addr, size_t __len, int __advice) __attribute__ ((__nothrow__ , __leaf__));
extern int posix_madvise (void *__addr, size_t __len, int __advice) __attribute__ ((__nothrow__ , __leaf__));
extern int mlock (const void *__addr, size_t __len) __attribute__ ((__nothrow__ , __leaf__));
extern int munlock (const void *__addr, size_t __len) __attribute__ ((__nothrow__ , __leaf__));
extern int mlockall (int __flags) __attribute__ ((__nothrow__ , __leaf__));
extern int munlockall (void) __attribute__ ((__nothrow__ , __leaf__));
extern int mincore (void *__start, size_t __len, unsigned char *__vec)
     __attribute__ ((__nothrow__ , __leaf__));
extern int shm_open (const char *__name, int __oflag, mode_t __mode);
extern int shm_unlink (const char *__name);


struct stat
  {
    __dev_t st_dev;
    __ino_t st_ino;
    __nlink_t st_nlink;
    __mode_t st_mode;
    __uid_t st_uid;
    __gid_t st_gid;
    int __pad0;
    __dev_t st_rdev;
    __off_t st_size;
    __blksize_t st_blksize;
    __blkcnt_t st_blocks;
    struct timespec st_atim;
    struct timespec st_mtim;
    struct timespec st_ctim;
    __syscall_slong_t __glibc_reserved[3];
  };
extern int stat (const char *__restrict __file,
   struct stat *__restrict __buf) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int fstat (int __fd, struct stat *__buf) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int fstatat (int __fd, const char *__restrict __file,
      struct stat *__restrict __buf, int __flag)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 3)));
extern int lstat (const char *__restrict __file,
    struct stat *__restrict __buf) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int chmod (const char *__file, __mode_t __mode)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int lchmod (const char *__file, __mode_t __mode)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int fchmod (int __fd, __mode_t __mode) __attribute__ ((__nothrow__ , __leaf__));
extern int fchmodat (int __fd, const char *__file, __mode_t __mode,
       int __flag)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2))) ;
extern __mode_t umask (__mode_t __mask) __attribute__ ((__nothrow__ , __leaf__));
extern int mkdir (const char *__path, __mode_t __mode)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int mkdirat (int __fd, const char *__path, __mode_t __mode)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int mknod (const char *__path, __mode_t __mode, __dev_t __dev)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int mknodat (int __fd, const char *__path, __mode_t __mode,
      __dev_t __dev) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int mkfifo (const char *__path, __mode_t __mode)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int mkfifoat (int __fd, const char *__path, __mode_t __mode)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int utimensat (int __fd, const char *__path,
        const struct timespec __times[2],
        int __flags)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int futimens (int __fd, const struct timespec __times[2]) __attribute__ ((__nothrow__ , __leaf__));
extern int __fxstat (int __ver, int __fildes, struct stat *__stat_buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3)));
extern int __xstat (int __ver, const char *__filename,
      struct stat *__stat_buf) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 3)));
extern int __lxstat (int __ver, const char *__filename,
       struct stat *__stat_buf) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 3)));
extern int __fxstatat (int __ver, int __fildes, const char *__filename,
         struct stat *__stat_buf, int __flag)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 4)));
extern int __xmknod (int __ver, const char *__path, __mode_t __mode,
       __dev_t *__dev) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 4)));
extern int __xmknodat (int __ver, int __fd, const char *__path,
         __mode_t __mode, __dev_t *__dev)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (3, 5)));


struct flock
  {
    short int l_type;
    short int l_whence;
    __off_t l_start;
    __off_t l_len;
    __pid_t l_pid;
  };


extern int fcntl (int __fd, int __cmd, ...);
extern int open (const char *__file, int __oflag, ...) __attribute__ ((__nonnull__ (1)));
extern int openat (int __fd, const char *__file, int __oflag, ...)
     __attribute__ ((__nonnull__ (2)));
extern int creat (const char *__file, mode_t __mode) __attribute__ ((__nonnull__ (1)));
extern int lockf (int __fd, int __cmd, off_t __len);
extern int posix_fadvise (int __fd, off_t __offset, off_t __len,
     int __advise) __attribute__ ((__nothrow__ , __leaf__));
extern int posix_fallocate (int __fd, off_t __offset, off_t __len);


typedef __useconds_t useconds_t;
typedef __socklen_t socklen_t;
extern int access (const char *__name, int __type) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int faccessat (int __fd, const char *__file, int __type, int __flag)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2))) ;
extern __off_t lseek (int __fd, __off_t __offset, int __whence) __attribute__ ((__nothrow__ , __leaf__));
extern int close (int __fd);
extern ssize_t read (int __fd, void *__buf, size_t __nbytes) ;
extern ssize_t write (int __fd, const void *__buf, size_t __n) ;
extern ssize_t pread (int __fd, void *__buf, size_t __nbytes,
        __off_t __offset) ;
extern ssize_t pwrite (int __fd, const void *__buf, size_t __n,
         __off_t __offset) ;
extern int pipe (int __pipedes[2]) __attribute__ ((__nothrow__ , __leaf__)) ;
extern unsigned int alarm (unsigned int __seconds) __attribute__ ((__nothrow__ , __leaf__));
extern unsigned int sleep (unsigned int __seconds);
extern __useconds_t ualarm (__useconds_t __value, __useconds_t __interval)
     __attribute__ ((__nothrow__ , __leaf__));
extern int usleep (__useconds_t __useconds);
extern int pause (void);
extern int chown (const char *__file, __uid_t __owner, __gid_t __group)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int fchown (int __fd, __uid_t __owner, __gid_t __group) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int lchown (const char *__file, __uid_t __owner, __gid_t __group)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int fchownat (int __fd, const char *__file, __uid_t __owner,
       __gid_t __group, int __flag)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2))) ;
extern int chdir (const char *__path) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int fchdir (int __fd) __attribute__ ((__nothrow__ , __leaf__)) ;
extern char *getcwd (char *__buf, size_t __size) __attribute__ ((__nothrow__ , __leaf__)) ;
extern char *getwd (char *__buf)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) __attribute__ ((__deprecated__)) ;
extern int dup (int __fd) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int dup2 (int __fd, int __fd2) __attribute__ ((__nothrow__ , __leaf__));
extern char **__environ;
extern int execve (const char *__path, char *const __argv[],
     char *const __envp[]) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int fexecve (int __fd, char *const __argv[], char *const __envp[])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int execv (const char *__path, char *const __argv[])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int execle (const char *__path, const char *__arg, ...)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int execl (const char *__path, const char *__arg, ...)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int execvp (const char *__file, char *const __argv[])
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int execlp (const char *__file, const char *__arg, ...)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern int nice (int __inc) __attribute__ ((__nothrow__ , __leaf__)) ;
extern void _exit (int __status) __attribute__ ((__noreturn__));
enum
  {
    _PC_LINK_MAX,
    _PC_MAX_CANON,
    _PC_MAX_INPUT,
    _PC_NAME_MAX,
    _PC_PATH_MAX,
    _PC_PIPE_BUF,
    _PC_CHOWN_RESTRICTED,
    _PC_NO_TRUNC,
    _PC_VDISABLE,
    _PC_SYNC_IO,
    _PC_ASYNC_IO,
    _PC_PRIO_IO,
    _PC_SOCK_MAXBUF,
    _PC_FILESIZEBITS,
    _PC_REC_INCR_XFER_SIZE,
    _PC_REC_MAX_XFER_SIZE,
    _PC_REC_MIN_XFER_SIZE,
    _PC_REC_XFER_ALIGN,
    _PC_ALLOC_SIZE_MIN,
    _PC_SYMLINK_MAX,
    _PC_2_SYMLINKS
  };
enum
  {
    _SC_ARG_MAX,
    _SC_CHILD_MAX,
    _SC_CLK_TCK,
    _SC_NGROUPS_MAX,
    _SC_OPEN_MAX,
    _SC_STREAM_MAX,
    _SC_TZNAME_MAX,
    _SC_JOB_CONTROL,
    _SC_SAVED_IDS,
    _SC_REALTIME_SIGNALS,
    _SC_PRIORITY_SCHEDULING,
    _SC_TIMERS,
    _SC_ASYNCHRONOUS_IO,
    _SC_PRIORITIZED_IO,
    _SC_SYNCHRONIZED_IO,
    _SC_FSYNC,
    _SC_MAPPED_FILES,
    _SC_MEMLOCK,
    _SC_MEMLOCK_RANGE,
    _SC_MEMORY_PROTECTION,
    _SC_MESSAGE_PASSING,
    _SC_SEMAPHORES,
    _SC_SHARED_MEMORY_OBJECTS,
    _SC_AIO_LISTIO_MAX,
    _SC_AIO_MAX,
    _SC_AIO_PRIO_DELTA_MAX,
    _SC_DELAYTIMER_MAX,
    _SC_MQ_OPEN_MAX,
    _SC_MQ_PRIO_MAX,
    _SC_VERSION,
    _SC_PAGESIZE,
    _SC_RTSIG_MAX,
    _SC_SEM_NSEMS_MAX,
    _SC_SEM_VALUE_MAX,
    _SC_SIGQUEUE_MAX,
    _SC_TIMER_MAX,
    _SC_BC_BASE_MAX,
    _SC_BC_DIM_MAX,
    _SC_BC_SCALE_MAX,
    _SC_BC_STRING_MAX,
    _SC_COLL_WEIGHTS_MAX,
    _SC_EQUIV_CLASS_MAX,
    _SC_EXPR_NEST_MAX,
    _SC_LINE_MAX,
    _SC_RE_DUP_MAX,
    _SC_CHARCLASS_NAME_MAX,
    _SC_2_VERSION,
    _SC_2_C_BIND,
    _SC_2_C_DEV,
    _SC_2_FORT_DEV,
    _SC_2_FORT_RUN,
    _SC_2_SW_DEV,
    _SC_2_LOCALEDEF,
    _SC_PII,
    _SC_PII_XTI,
    _SC_PII_SOCKET,
    _SC_PII_INTERNET,
    _SC_PII_OSI,
    _SC_POLL,
    _SC_SELECT,
    _SC_UIO_MAXIOV,
    _SC_IOV_MAX = _SC_UIO_MAXIOV,
    _SC_PII_INTERNET_STREAM,
    _SC_PII_INTERNET_DGRAM,
    _SC_PII_OSI_COTS,
    _SC_PII_OSI_CLTS,
    _SC_PII_OSI_M,
    _SC_T_IOV_MAX,
    _SC_THREADS,
    _SC_THREAD_SAFE_FUNCTIONS,
    _SC_GETGR_R_SIZE_MAX,
    _SC_GETPW_R_SIZE_MAX,
    _SC_LOGIN_NAME_MAX,
    _SC_TTY_NAME_MAX,
    _SC_THREAD_DESTRUCTOR_ITERATIONS,
    _SC_THREAD_KEYS_MAX,
    _SC_THREAD_STACK_MIN,
    _SC_THREAD_THREADS_MAX,
    _SC_THREAD_ATTR_STACKADDR,
    _SC_THREAD_ATTR_STACKSIZE,
    _SC_THREAD_PRIORITY_SCHEDULING,
    _SC_THREAD_PRIO_INHERIT,
    _SC_THREAD_PRIO_PROTECT,
    _SC_THREAD_PROCESS_SHARED,
    _SC_NPROCESSORS_CONF,
    _SC_NPROCESSORS_ONLN,
    _SC_PHYS_PAGES,
    _SC_AVPHYS_PAGES,
    _SC_ATEXIT_MAX,
    _SC_PASS_MAX,
    _SC_XOPEN_VERSION,
    _SC_XOPEN_XCU_VERSION,
    _SC_XOPEN_UNIX,
    _SC_XOPEN_CRYPT,
    _SC_XOPEN_ENH_I18N,
    _SC_XOPEN_SHM,
    _SC_2_CHAR_TERM,
    _SC_2_C_VERSION,
    _SC_2_UPE,
    _SC_XOPEN_XPG2,
    _SC_XOPEN_XPG3,
    _SC_XOPEN_XPG4,
    _SC_CHAR_BIT,
    _SC_CHAR_MAX,
    _SC_CHAR_MIN,
    _SC_INT_MAX,
    _SC_INT_MIN,
    _SC_LONG_BIT,
    _SC_WORD_BIT,
    _SC_MB_LEN_MAX,
    _SC_NZERO,
    _SC_SSIZE_MAX,
    _SC_SCHAR_MAX,
    _SC_SCHAR_MIN,
    _SC_SHRT_MAX,
    _SC_SHRT_MIN,
    _SC_UCHAR_MAX,
    _SC_UINT_MAX,
    _SC_ULONG_MAX,
    _SC_USHRT_MAX,
    _SC_NL_ARGMAX,
    _SC_NL_LANGMAX,
    _SC_NL_MSGMAX,
    _SC_NL_NMAX,
    _SC_NL_SETMAX,
    _SC_NL_TEXTMAX,
    _SC_XBS5_ILP32_OFF32,
    _SC_XBS5_ILP32_OFFBIG,
    _SC_XBS5_LP64_OFF64,
    _SC_XBS5_LPBIG_OFFBIG,
    _SC_XOPEN_LEGACY,
    _SC_XOPEN_REALTIME,
    _SC_XOPEN_REALTIME_THREADS,
    _SC_ADVISORY_INFO,
    _SC_BARRIERS,
    _SC_BASE,
    _SC_C_LANG_SUPPORT,
    _SC_C_LANG_SUPPORT_R,
    _SC_CLOCK_SELECTION,
    _SC_CPUTIME,
    _SC_THREAD_CPUTIME,
    _SC_DEVICE_IO,
    _SC_DEVICE_SPECIFIC,
    _SC_DEVICE_SPECIFIC_R,
    _SC_FD_MGMT,
    _SC_FIFO,
    _SC_PIPE,
    _SC_FILE_ATTRIBUTES,
    _SC_FILE_LOCKING,
    _SC_FILE_SYSTEM,
    _SC_MONOTONIC_CLOCK,
    _SC_MULTI_PROCESS,
    _SC_SINGLE_PROCESS,
    _SC_NETWORKING,
    _SC_READER_WRITER_LOCKS,
    _SC_SPIN_LOCKS,
    _SC_REGEXP,
    _SC_REGEX_VERSION,
    _SC_SHELL,
    _SC_SIGNALS,
    _SC_SPAWN,
    _SC_SPORADIC_SERVER,
    _SC_THREAD_SPORADIC_SERVER,
    _SC_SYSTEM_DATABASE,
    _SC_SYSTEM_DATABASE_R,
    _SC_TIMEOUTS,
    _SC_TYPED_MEMORY_OBJECTS,
    _SC_USER_GROUPS,
    _SC_USER_GROUPS_R,
    _SC_2_PBS,
    _SC_2_PBS_ACCOUNTING,
    _SC_2_PBS_LOCATE,
    _SC_2_PBS_MESSAGE,
    _SC_2_PBS_TRACK,
    _SC_SYMLOOP_MAX,
    _SC_STREAMS,
    _SC_2_PBS_CHECKPOINT,
    _SC_V6_ILP32_OFF32,
    _SC_V6_ILP32_OFFBIG,
    _SC_V6_LP64_OFF64,
    _SC_V6_LPBIG_OFFBIG,
    _SC_HOST_NAME_MAX,
    _SC_TRACE,
    _SC_TRACE_EVENT_FILTER,
    _SC_TRACE_INHERIT,
    _SC_TRACE_LOG,
    _SC_LEVEL1_ICACHE_SIZE,
    _SC_LEVEL1_ICACHE_ASSOC,
    _SC_LEVEL1_ICACHE_LINESIZE,
    _SC_LEVEL1_DCACHE_SIZE,
    _SC_LEVEL1_DCACHE_ASSOC,
    _SC_LEVEL1_DCACHE_LINESIZE,
    _SC_LEVEL2_CACHE_SIZE,
    _SC_LEVEL2_CACHE_ASSOC,
    _SC_LEVEL2_CACHE_LINESIZE,
    _SC_LEVEL3_CACHE_SIZE,
    _SC_LEVEL3_CACHE_ASSOC,
    _SC_LEVEL3_CACHE_LINESIZE,
    _SC_LEVEL4_CACHE_SIZE,
    _SC_LEVEL4_CACHE_ASSOC,
    _SC_LEVEL4_CACHE_LINESIZE,
    _SC_IPV6 = _SC_LEVEL1_ICACHE_SIZE + 50,
    _SC_RAW_SOCKETS,
    _SC_V7_ILP32_OFF32,
    _SC_V7_ILP32_OFFBIG,
    _SC_V7_LP64_OFF64,
    _SC_V7_LPBIG_OFFBIG,
    _SC_SS_REPL_MAX,
    _SC_TRACE_EVENT_NAME_MAX,
    _SC_TRACE_NAME_MAX,
    _SC_TRACE_SYS_MAX,
    _SC_TRACE_USER_EVENT_MAX,
    _SC_XOPEN_STREAMS,
    _SC_THREAD_ROBUST_PRIO_INHERIT,
    _SC_THREAD_ROBUST_PRIO_PROTECT
  };
enum
  {
    _CS_PATH,
    _CS_V6_WIDTH_RESTRICTED_ENVS,
    _CS_GNU_LIBC_VERSION,
    _CS_GNU_LIBPTHREAD_VERSION,
    _CS_V5_WIDTH_RESTRICTED_ENVS,
    _CS_V7_WIDTH_RESTRICTED_ENVS,
    _CS_LFS_CFLAGS = 1000,
    _CS_LFS_LDFLAGS,
    _CS_LFS_LIBS,
    _CS_LFS_LINTFLAGS,
    _CS_LFS64_CFLAGS,
    _CS_LFS64_LDFLAGS,
    _CS_LFS64_LIBS,
    _CS_LFS64_LINTFLAGS,
    _CS_XBS5_ILP32_OFF32_CFLAGS = 1100,
    _CS_XBS5_ILP32_OFF32_LDFLAGS,
    _CS_XBS5_ILP32_OFF32_LIBS,
    _CS_XBS5_ILP32_OFF32_LINTFLAGS,
    _CS_XBS5_ILP32_OFFBIG_CFLAGS,
    _CS_XBS5_ILP32_OFFBIG_LDFLAGS,
    _CS_XBS5_ILP32_OFFBIG_LIBS,
    _CS_XBS5_ILP32_OFFBIG_LINTFLAGS,
    _CS_XBS5_LP64_OFF64_CFLAGS,
    _CS_XBS5_LP64_OFF64_LDFLAGS,
    _CS_XBS5_LP64_OFF64_LIBS,
    _CS_XBS5_LP64_OFF64_LINTFLAGS,
    _CS_XBS5_LPBIG_OFFBIG_CFLAGS,
    _CS_XBS5_LPBIG_OFFBIG_LDFLAGS,
    _CS_XBS5_LPBIG_OFFBIG_LIBS,
    _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS,
    _CS_POSIX_V6_ILP32_OFF32_CFLAGS,
    _CS_POSIX_V6_ILP32_OFF32_LDFLAGS,
    _CS_POSIX_V6_ILP32_OFF32_LIBS,
    _CS_POSIX_V6_ILP32_OFF32_LINTFLAGS,
    _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS,
    _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS,
    _CS_POSIX_V6_ILP32_OFFBIG_LIBS,
    _CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS,
    _CS_POSIX_V6_LP64_OFF64_CFLAGS,
    _CS_POSIX_V6_LP64_OFF64_LDFLAGS,
    _CS_POSIX_V6_LP64_OFF64_LIBS,
    _CS_POSIX_V6_LP64_OFF64_LINTFLAGS,
    _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS,
    _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS,
    _CS_POSIX_V6_LPBIG_OFFBIG_LIBS,
    _CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS,
    _CS_POSIX_V7_ILP32_OFF32_CFLAGS,
    _CS_POSIX_V7_ILP32_OFF32_LDFLAGS,
    _CS_POSIX_V7_ILP32_OFF32_LIBS,
    _CS_POSIX_V7_ILP32_OFF32_LINTFLAGS,
    _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS,
    _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS,
    _CS_POSIX_V7_ILP32_OFFBIG_LIBS,
    _CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS,
    _CS_POSIX_V7_LP64_OFF64_CFLAGS,
    _CS_POSIX_V7_LP64_OFF64_LDFLAGS,
    _CS_POSIX_V7_LP64_OFF64_LIBS,
    _CS_POSIX_V7_LP64_OFF64_LINTFLAGS,
    _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS,
    _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS,
    _CS_POSIX_V7_LPBIG_OFFBIG_LIBS,
    _CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS,
    _CS_V6_ENV,
    _CS_V7_ENV
  };
extern long int pathconf (const char *__path, int __name)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern long int fpathconf (int __fd, int __name) __attribute__ ((__nothrow__ , __leaf__));
extern long int sysconf (int __name) __attribute__ ((__nothrow__ , __leaf__));
extern size_t confstr (int __name, char *__buf, size_t __len) __attribute__ ((__nothrow__ , __leaf__));
extern __pid_t getpid (void) __attribute__ ((__nothrow__ , __leaf__));
extern __pid_t getppid (void) __attribute__ ((__nothrow__ , __leaf__));
extern __pid_t getpgrp (void) __attribute__ ((__nothrow__ , __leaf__));
extern __pid_t __getpgid (__pid_t __pid) __attribute__ ((__nothrow__ , __leaf__));
extern __pid_t getpgid (__pid_t __pid) __attribute__ ((__nothrow__ , __leaf__));
extern int setpgid (__pid_t __pid, __pid_t __pgid) __attribute__ ((__nothrow__ , __leaf__));
extern int setpgrp (void) __attribute__ ((__nothrow__ , __leaf__));
extern __pid_t setsid (void) __attribute__ ((__nothrow__ , __leaf__));
extern __pid_t getsid (__pid_t __pid) __attribute__ ((__nothrow__ , __leaf__));
extern __uid_t getuid (void) __attribute__ ((__nothrow__ , __leaf__));
extern __uid_t geteuid (void) __attribute__ ((__nothrow__ , __leaf__));
extern __gid_t getgid (void) __attribute__ ((__nothrow__ , __leaf__));
extern __gid_t getegid (void) __attribute__ ((__nothrow__ , __leaf__));
extern int getgroups (int __size, __gid_t __list[]) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int setuid (__uid_t __uid) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int setreuid (__uid_t __ruid, __uid_t __euid) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int seteuid (__uid_t __uid) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int setgid (__gid_t __gid) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int setregid (__gid_t __rgid, __gid_t __egid) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int setegid (__gid_t __gid) __attribute__ ((__nothrow__ , __leaf__)) ;
extern __pid_t fork (void) __attribute__ ((__nothrow__));
extern __pid_t vfork (void) __attribute__ ((__nothrow__ , __leaf__));
extern char *ttyname (int __fd) __attribute__ ((__nothrow__ , __leaf__));
extern int ttyname_r (int __fd, char *__buf, size_t __buflen)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2))) ;
extern int isatty (int __fd) __attribute__ ((__nothrow__ , __leaf__));
extern int ttyslot (void) __attribute__ ((__nothrow__ , __leaf__));
extern int link (const char *__from, const char *__to)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2))) ;
extern int linkat (int __fromfd, const char *__from, int __tofd,
     const char *__to, int __flags)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 4))) ;
extern int symlink (const char *__from, const char *__to)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2))) ;
extern ssize_t readlink (const char *__restrict __path,
    char *__restrict __buf, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2))) ;
extern int symlinkat (const char *__from, int __tofd,
        const char *__to) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 3))) ;
extern ssize_t readlinkat (int __fd, const char *__restrict __path,
      char *__restrict __buf, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2, 3))) ;
extern int unlink (const char *__name) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int unlinkat (int __fd, const char *__name, int __flag)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (2)));
extern int rmdir (const char *__path) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern __pid_t tcgetpgrp (int __fd) __attribute__ ((__nothrow__ , __leaf__));
extern int tcsetpgrp (int __fd, __pid_t __pgrp_id) __attribute__ ((__nothrow__ , __leaf__));
extern char *getlogin (void);
extern int getlogin_r (char *__name, size_t __name_len) __attribute__ ((__nonnull__ (1)));
extern int setlogin (const char *__name) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern char *optarg;
extern int optind;
extern int opterr;
extern int optopt;
extern int getopt (int ___argc, char *const *___argv, const char *__shortopts)
       __attribute__ ((__nothrow__ , __leaf__));
extern int gethostname (char *__name, size_t __len) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int sethostname (const char *__name, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int sethostid (long int __id) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int getdomainname (char *__name, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int setdomainname (const char *__name, size_t __len)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int vhangup (void) __attribute__ ((__nothrow__ , __leaf__));
extern int revoke (const char *__file) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int profil (unsigned short int *__sample_buffer, size_t __size,
     size_t __offset, unsigned int __scale)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int acct (const char *__name) __attribute__ ((__nothrow__ , __leaf__));
extern char *getusershell (void) __attribute__ ((__nothrow__ , __leaf__));
extern void endusershell (void) __attribute__ ((__nothrow__ , __leaf__));
extern void setusershell (void) __attribute__ ((__nothrow__ , __leaf__));
extern int daemon (int __nochdir, int __noclose) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int chroot (const char *__path) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern char *getpass (const char *__prompt) __attribute__ ((__nonnull__ (1)));
extern int fsync (int __fd);
extern long int gethostid (void);
extern void sync (void) __attribute__ ((__nothrow__ , __leaf__));
extern int getpagesize (void) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
extern int getdtablesize (void) __attribute__ ((__nothrow__ , __leaf__));
extern int truncate (const char *__file, __off_t __length)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1))) ;
extern int ftruncate (int __fd, __off_t __length) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int brk (void *__addr) __attribute__ ((__nothrow__ , __leaf__)) ;
extern void *sbrk (intptr_t __delta) __attribute__ ((__nothrow__ , __leaf__));
extern long int syscall (long int __sysno, ...) __attribute__ ((__nothrow__ , __leaf__));
extern int fdatasync (int __fildes);

typedef struct ObjectRecord
{
 void (*destroy)(void);
 size_t codePageSize;
 unsigned char *codePage;
 unsigned char *codePagePtr;
} Object;
typedef struct vtable
{
 int __chk_v;
 char *__func;
 void (*vptr)();
} vtable;
extern void *Object_trampoline(Object *, void *, int);
extern void Object_destroy(Object *);
extern void *Object_create(size_t, int);
extern void Object_prepare(Object *);
extern void callprotect(Object *);
extern void *Object_stacked(Object *, int);
extern char *isPtrOnStack(void *);
extern uint8_t isList(char *);
extern void *getListBase(const char *);
extern void *getStdPtrBase(const char *);
extern void *Object_stackAlloc(size_t, int);
extern void __initialize_virtual_table(vtable **, int *);
extern void __register_virtual_method(vtable *, int *, char *, void (*)(), Object *);
extern uint8_t __chk_func_override(char *, char *);
extern void __register_overriden_method(vtable *, void *, int *, char *, int, void *, Object *);
static const unsigned char kTrampoline[] = {
 0x48, 0xbf, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
 0xff, 0x25, 0x00, 0x00, 0x00, 0x00,
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
typedef struct StringPrivate StringPrivate;
typedef struct string
{
 Object object;
 struct StringPrivate *StringPrivate;;
 int (*CompareTo)(const char *);
 int (*Length)(void);
    struct string *(*Concatenate)(char *);
    char *(*Get)(void);
 struct string *(*ToString)(void);
 void *(*String0)(void);
 void *(*String1)(const char *);
 void *(*String2)(const char *, size_t);
} String;
extern int String_CompareTo(const char *, String * const);
extern int String_Length(String * const);
extern String *String_Concatenate(const char *, String * const);
extern char *String_GetString(String * const);
extern String *String_ToString(String * const);
extern void *String_String0(String *);
extern void *String_String1(const char *, String *);
extern void *String_String2(const char *, size_t, String *);
typedef struct Int16Private Int16Private;
typedef struct int16
{
    Object object;
    struct Int16Private *Int16Private;;
    short (*Get)(void);
    struct int16 *(*Parse)(struct string *);
    struct string *(*ToString)(void);
    uint8_t (*ReferenceEquality)(void *);
    uint8_t (*Equals)(struct int16 *);
    void *(*Int160)(void);
    void *(*Int161)(short);
} Int16;
extern short Int16_Get(Int16 * const);
extern Int16 *Int16_Parse(struct string *, Int16 * const);
extern struct string *Int16_ToString(Int16 * const);
extern uint8_t Int16_ReferenceEquality(void *, Int16 * const);
extern uint8_t Int16_Equals(Int16 *, Int16 * const);
extern void *Int16_Int160(Int16 *);
extern void *Int16_Int161(short, Int16 *);
typedef struct Int32Private Int32Private;
typedef struct int32
{
    Object object;
    struct Int32Private *Int32Private;;
    int (*Get)(void);
    struct int32 *(*Parse)(struct string *);
    struct string *(*ToString)(void);
    uint8_t (*ReferenceEquality)(void *);
    uint8_t (*Equals)(struct int32 *);
    void *(*Int320)(void);
    void *(*Int321)(int);
} Int32;
extern int Int32_Get(Int32 * const);
extern Int32 *Int32_Parse(struct string *, Int32 * const);
extern struct string *Int32_ToString(Int32 * const);
extern uint8_t Int32_ReferenceEquality(void *, Int32 * const);
extern uint8_t Int32_Equals(Int32 *, Int32 * const);
extern void *Int32_Int320(Int32 *);
extern void *Int32_Int321(int, Int32 *);
typedef struct Int64Private Int64Private;
typedef struct int64
{
    Object object;
    struct Int64Private *Int64Private;;
    long (*Get)(void);
    struct int64 *(*Parse)(struct string *);
    struct string *(*ToString)(void);
    uint8_t (*ReferenceEquality)(void *);
    uint8_t (*Equals)(struct int64 *);
    void *(*Int640)(void);
    void *(*Int641)(long);
} Int64;
extern long Int64_Get(Int64 * const);
extern Int64 *Int64_Parse(struct string *, Int64 * const);
extern struct string *Int64_ToString(Int64 * const);
extern uint8_t Int64_ReferenceEquality(void *, Int64 * const);
extern uint8_t Int64_Equals(Int64 *, Int64 * const);
extern void *Int64_Int640(Int64 *);
extern void *Int64_Int641(long, Int64 *);
typedef struct UInt16Private UInt16Private;
typedef struct uint16
{
    Object object;
    struct UInt16Private *UInt16Private;;
    short (*Get)(void);
    struct Uint16 *(*Parse)(struct string *);
    struct string *(*ToString)(void);
    uint8_t (*ReferenceEquality)(void *);
    uint8_t (*Equals)(struct uint16 *);
    void *(*UInt160)(void);
    void *(*UInt161)(short);
} UInt16;
extern unsigned short UInt16_Get(UInt16 * const);
extern UInt16 *UInt16_Parse(struct string *, UInt16 * const);
extern struct string *UInt16_ToString(UInt16 * const);
extern uint8_t UInt16_ReferenceEquality(void *, UInt16 * const);
extern uint8_t UInt16_Equals(UInt16 *, UInt16 * const);
extern void *UInt16_UInt160(UInt16 *);
extern void *UInt16_UInt161(unsigned short, UInt16 *);
typedef struct UInt32Private UInt32Private;
typedef struct uint32
{
    Object object;
    struct UInt32Private *UInt32Private;;
    short (*Get)(void);
    struct Uint32 *(*Parse)(struct string *);
    struct string *(*ToString)(void);
    uint8_t (*ReferenceEquality)(void *);
    uint8_t (*Equals)(struct uint32 *);
    void *(*UInt320)(void);
    void *(*UInt321)(int);
} UInt32;
extern unsigned int UInt32_Get(UInt32 * const);
extern UInt32 *UInt32_Parse(struct string *, UInt32 * const);
extern struct string *UInt32_ToString(UInt32 * const);
extern uint8_t UInt32_ReferenceEquality(void *, UInt32 * const);
extern uint8_t UInt32_Equals(UInt32 *, UInt32 * const);
extern void *UInt32_UInt320(UInt32 *);
extern void *UInt32_UInt321(unsigned int, UInt32 *);
typedef struct UInt64Private UInt64Private;
typedef struct uint64
{
    Object object;
    struct UInt64Private *UInt64Private;;
    unsigned long (*Get)(void);
    struct Uint64 *(*Parse)(struct string *);
    struct string *(*ToString)(void);
    uint8_t (*ReferenceEquality)(void *);
    uint8_t (*Equals)(struct uint64 *);
    void *(*UInt640)(void);
    void *(*UInt641)(unsigned long);
} UInt64;
extern unsigned long UInt64_Get(UInt64 * const);
extern UInt64 *UInt64_Parse(struct string *, UInt64 * const);
extern struct string *UInt64_ToString(UInt64 * const);
extern uint8_t UInt64_ReferenceEquality(void *, UInt64 * const);
extern uint8_t UInt64_Equals(UInt64 *, UInt64 * const);
extern void *UInt64_UInt640(UInt64 *);
extern void *UInt64_UInt641(unsigned long, UInt64 *);
typedef struct CharPrivate CharPrivate;
typedef struct charClass
{
    Object object;
    struct CharPrivate *CharPrivate;;
    char (*Get)(void);
    int (*CompareTo)(struct charClass *);
    uint8_t (*Equals)(struct charClass *);
    struct string *(*GetType)(void);
    uint8_t (*IsDigit)(struct charClass *);
    uint8_t (*IsLetter)(struct charClass *);
    uint8_t (*IsLower)(struct charClass *);
    uint8_t (*IsUpper)(struct charClass *);
    struct charClass (*ToLower)(struct charClass *);
    struct charClass (*ToUpper)(struct charClass *);
    struct string *(*ToString)(void);
    void *(*Char0)(void);
    void *(*Char1)(const char);
} Char;
extern int Char_CompareTo(Char *, Char *);
extern uint8_t Char_Equals(Char *, Char *);
extern struct string *Char_GetType(Char *);
extern uint8_t Char_IsDigit(Char *, Char *);
extern uint8_t Char_IsLetter(Char *, Char *);
extern uint8_t Char_IsLower(Char *, Char *);
extern uint8_t Char_IsUpper(Char *, Char *);
extern Char *Char_ToLower(Char *, Char *);
extern Char *Char_ToUpper(Char *, Char *);
extern struct string *Char_ToString(Char *);
extern void *Char_Char0(Char *);
extern void *Char_Char1(const char, Char *);
typedef struct FloatPrivate FloatPrivate;
typedef struct floatClass
{
    Object object;
    struct FloatPrivate *FloatPrivate;;
    float (*Get)(void);
    uint8_t (*Equals)(struct floatClass *);
    struct string *(*ToString)(void);
    char *(*GetType)(void);
    void *(*Float0)(void);
    void *(*Float1)(const float);
} Float;
extern float Float_Get(Float *);
extern uint8_t Float_Equals(Float *, Float *);
extern struct string *Float_ToString(Float *);
extern struct string *Float_GetType(Float *);
extern void *Float_Float0(Float *);
extern void *Float_Float1(const float, Float *);
typedef struct DoublePrivate DoublePrivate;
typedef struct doubleClass
{
    Object object;
    struct DoublePrivate *DoublePrivate;;
    double (*Get)(void);
    uint8_t (*Equals)(struct doubleClass *);
    struct doubleClass *(*Parse)(struct string *);
    struct string *(*ToString)(void);
    char *(*GetType)(void);
    void *(*Double0)(void);
    void *(*Double1)(double);
} Double;
extern double Double_Get(Double * const);
extern uint8_t Double_Equals(Double *, Double * const);
extern Double *Double_Parse(struct string *, Double * const);
extern struct string *Double_ToString(Double * const);
extern char *Double_GetType(Double * const);
extern void *Double_Double0(Double *);
extern void *Double_Double1(double, Double *);
typedef enum fmode
{
    None = 0,
    Read,
    Write,
    Append,
    ReadUpdate,
    WriteUpdate,
    AppendUpdate
} FileMode;
typedef struct FileStreamPrivate FileStreamPrivate;
typedef struct file
{
    Object object;
    struct FileStreamPrivate *FileStreamPrivate;;
    FILE *(*Get)(void);
    void (*StreamOpen)(struct string *, FileMode);
    void (*StreamClose)(void);
    void (*StreamFlush)(void);
    char (*GetChar)(void);
    struct string *(*GetString)(struct string *, size_t);
    void (*PutChar)(char);
    void (*PutString)(struct string *);
    void *(*File0)(void);
    void *(*File1)(const struct file *, FileMode);
} FileStream;
extern FILE *FileStream_Get(FileStream * const );
extern void FileStream_StreamOpen(struct string *, FileMode, FileStream * const);
extern void FileStream_StreamClose(FileStream * const);
extern void FileStream_StreamFlush(FileStream * const);
extern char FileStream_GetChar(FileStream * const);
extern struct string *FileStream_GetString(struct string *, size_t, FileStream * const);
extern void FileStream_PutChar(char, FileStream * const);
extern void FileStream_PutString(struct string *, FileStream * const);
extern void *FileStream_File0(FileStream *);
extern void *FileStream_File1(FileStream *, FileMode, FileStream *);
typedef struct List_Int16Private List_Int16Private; typedef struct lista_Int16 { Object object; struct List_Int16Private *List_Int16Private;; const struct readonlylist_Int16 *(*AsReadOnly)(void); uint8_t (*Append)(Int16 *); uint8_t (*Delete)(const int); void (*Reverse)(void); void (*Show)(void); void (*Clear)(void); int (*Count)(void); int (*BinarySearch)(Int16 *); char *(*GetType)(void); Int16 **(*ToArray)(void); } List_Int16;;
typedef struct List_Int32Private List_Int32Private; typedef struct lista_Int32 { Object object; struct List_Int32Private *List_Int32Private;; const struct readonlylist_Int32 *(*AsReadOnly)(void); uint8_t (*Append)(Int32 *); uint8_t (*Delete)(const int); void (*Reverse)(void); void (*Show)(void); void (*Clear)(void); int (*Count)(void); int (*BinarySearch)(Int32 *); char *(*GetType)(void); Int32 **(*ToArray)(void); } List_Int32;;
typedef struct List_Int64Private List_Int64Private; typedef struct lista_Int64 { Object object; struct List_Int64Private *List_Int64Private;; const struct readonlylist_Int64 *(*AsReadOnly)(void); uint8_t (*Append)(Int64 *); uint8_t (*Delete)(const int); void (*Reverse)(void); void (*Show)(void); void (*Clear)(void); int (*Count)(void); int (*BinarySearch)(Int64 *); char *(*GetType)(void); Int64 **(*ToArray)(void); } List_Int64;;
typedef struct List_UInt16Private List_UInt16Private; typedef struct lista_UInt16 { Object object; struct List_UInt16Private *List_UInt16Private;; const struct readonlylist_UInt16 *(*AsReadOnly)(void); uint8_t (*Append)(UInt16 *); uint8_t (*Delete)(const int); void (*Reverse)(void); void (*Show)(void); void (*Clear)(void); int (*Count)(void); int (*BinarySearch)(UInt16 *); char *(*GetType)(void); UInt16 **(*ToArray)(void); } List_UInt16;;
typedef struct List_UInt32Private List_UInt32Private; typedef struct lista_UInt32 { Object object; struct List_UInt32Private *List_UInt32Private;; const struct readonlylist_UInt32 *(*AsReadOnly)(void); uint8_t (*Append)(UInt32 *); uint8_t (*Delete)(const int); void (*Reverse)(void); void (*Show)(void); void (*Clear)(void); int (*Count)(void); int (*BinarySearch)(UInt32 *); char *(*GetType)(void); UInt32 **(*ToArray)(void); } List_UInt32;;
typedef struct List_UInt64Private List_UInt64Private; typedef struct lista_UInt64 { Object object; struct List_UInt64Private *List_UInt64Private;; const struct readonlylist_UInt64 *(*AsReadOnly)(void); uint8_t (*Append)(UInt64 *); uint8_t (*Delete)(const int); void (*Reverse)(void); void (*Show)(void); void (*Clear)(void); int (*Count)(void); int (*BinarySearch)(UInt64 *); char *(*GetType)(void); UInt64 **(*ToArray)(void); } List_UInt64;;
typedef struct List_StringPrivate List_StringPrivate; typedef struct lista_String { Object object; struct List_StringPrivate *List_StringPrivate;; const struct readonlylist_String *(*AsReadOnly)(void); uint8_t (*Append)(String *); uint8_t (*Delete)(const int); void (*Reverse)(void); void (*Show)(void); void (*Clear)(void); int (*Count)(void); int (*BinarySearch)(String *); char *(*GetType)(void); String **(*ToArray)(void); } List_String;;
typedef struct ReadOnlyList_Int16Private ReadOnlyList_Int16Private; typedef struct readonlylist_Int16 { Object object; struct List_Int16Private *List_Int16Private;; char *(*GetType)(void); int (*IndexOf)(Int16 *); int (*Count)(void); uint8_t(*Contains)(Int16 *); uint8_t (*Equals)(void *); void (*CopyTo)(Int16 **, int); } ReadOnlyList_Int16; typedef struct StaticList_Int16Private StaticList_Int16Private; typedef struct staticlist_Int16 { Object object; struct List_Int16Private *List_Int16Private;; char *(*GetType)(void); int (*IndexOf)(Int16 *); int (*Count)(void); uint8_t (*Contains)(Int16 *); uint8_t (*Equals)(void *); void (*CopyTo)(Int16 **, int); } StaticList_Int16;;
typedef struct ReadOnlyList_Int32Private ReadOnlyList_Int32Private; typedef struct readonlylist_Int32 { Object object; struct List_Int32Private *List_Int32Private;; char *(*GetType)(void); int (*IndexOf)(Int32 *); int (*Count)(void); uint8_t(*Contains)(Int32 *); uint8_t (*Equals)(void *); void (*CopyTo)(Int32 **, int); } ReadOnlyList_Int32; typedef struct StaticList_Int32Private StaticList_Int32Private; typedef struct staticlist_Int32 { Object object; struct List_Int32Private *List_Int32Private;; char *(*GetType)(void); int (*IndexOf)(Int32 *); int (*Count)(void); uint8_t (*Contains)(Int32 *); uint8_t (*Equals)(void *); void (*CopyTo)(Int32 **, int); } StaticList_Int32;;
typedef struct ReadOnlyList_Int64Private ReadOnlyList_Int64Private; typedef struct readonlylist_Int64 { Object object; struct List_Int64Private *List_Int64Private;; char *(*GetType)(void); int (*IndexOf)(Int64 *); int (*Count)(void); uint8_t(*Contains)(Int64 *); uint8_t (*Equals)(void *); void (*CopyTo)(Int64 **, int); } ReadOnlyList_Int64; typedef struct StaticList_Int64Private StaticList_Int64Private; typedef struct staticlist_Int64 { Object object; struct List_Int64Private *List_Int64Private;; char *(*GetType)(void); int (*IndexOf)(Int64 *); int (*Count)(void); uint8_t (*Contains)(Int64 *); uint8_t (*Equals)(void *); void (*CopyTo)(Int64 **, int); } StaticList_Int64;;
typedef struct ReadOnlyList_UInt16Private ReadOnlyList_UInt16Private; typedef struct readonlylist_UInt16 { Object object; struct List_UInt16Private *List_UInt16Private;; char *(*GetType)(void); int (*IndexOf)(UInt16 *); int (*Count)(void); uint8_t(*Contains)(UInt16 *); uint8_t (*Equals)(void *); void (*CopyTo)(UInt16 **, int); } ReadOnlyList_UInt16; typedef struct StaticList_UInt16Private StaticList_UInt16Private; typedef struct staticlist_UInt16 { Object object; struct List_UInt16Private *List_UInt16Private;; char *(*GetType)(void); int (*IndexOf)(UInt16 *); int (*Count)(void); uint8_t (*Contains)(UInt16 *); uint8_t (*Equals)(void *); void (*CopyTo)(UInt16 **, int); } StaticList_UInt16;;
typedef struct ReadOnlyList_UInt32Private ReadOnlyList_UInt32Private; typedef struct readonlylist_UInt32 { Object object; struct List_UInt32Private *List_UInt32Private;; char *(*GetType)(void); int (*IndexOf)(UInt32 *); int (*Count)(void); uint8_t(*Contains)(UInt32 *); uint8_t (*Equals)(void *); void (*CopyTo)(UInt32 **, int); } ReadOnlyList_UInt32; typedef struct StaticList_UInt32Private StaticList_UInt32Private; typedef struct staticlist_UInt32 { Object object; struct List_UInt32Private *List_UInt32Private;; char *(*GetType)(void); int (*IndexOf)(UInt32 *); int (*Count)(void); uint8_t (*Contains)(UInt32 *); uint8_t (*Equals)(void *); void (*CopyTo)(UInt32 **, int); } StaticList_UInt32;;
typedef struct ReadOnlyList_UInt64Private ReadOnlyList_UInt64Private; typedef struct readonlylist_UInt64 { Object object; struct List_UInt64Private *List_UInt64Private;; char *(*GetType)(void); int (*IndexOf)(UInt64 *); int (*Count)(void); uint8_t(*Contains)(UInt64 *); uint8_t (*Equals)(void *); void (*CopyTo)(UInt64 **, int); } ReadOnlyList_UInt64; typedef struct StaticList_UInt64Private StaticList_UInt64Private; typedef struct staticlist_UInt64 { Object object; struct List_UInt64Private *List_UInt64Private;; char *(*GetType)(void); int (*IndexOf)(UInt64 *); int (*Count)(void); uint8_t (*Contains)(UInt64 *); uint8_t (*Equals)(void *); void (*CopyTo)(UInt64 **, int); } StaticList_UInt64;;
typedef struct ReadOnlyList_StringPrivate ReadOnlyList_StringPrivate; typedef struct readonlylist_String { Object object; struct List_StringPrivate *List_StringPrivate;; char *(*GetType)(void); int (*IndexOf)(String *); int (*Count)(void); uint8_t(*Contains)(String *); uint8_t (*Equals)(void *); void (*CopyTo)(String **, int); } ReadOnlyList_String; typedef struct StaticList_StringPrivate StaticList_StringPrivate; typedef struct staticlist_String { Object object; struct List_StringPrivate *List_StringPrivate;; char *(*GetType)(void); int (*IndexOf)(String *); int (*Count)(void); uint8_t (*Contains)(String *); uint8_t (*Equals)(void *); void (*CopyTo)(String **, int); } StaticList_String;;
extern void *NList_Int16(void);
extern void *NList_Int32(void);
extern void *NList_Int64(void);
extern void *NList_UInt16(void);
extern void *NList_UInt32(void);
extern void *NList_UInt64(void);
extern void *NList_String(void);
extern const void *NReadOnlyList_Int16(List_Int16 *);
extern const void *NReadOnlyList_Int32(List_Int32 *);
extern const void *NReadOnlyList_Int64(List_Int64 *);
extern const void *NReadOnlyList_UInt16(List_UInt16 *);
extern const void *NReadOnlyList_UInt32(List_UInt32 *);
extern const void *NReadOnlyList_UInt64(List_UInt64 *);
extern const void *NReadOnlyList_String(List_String *);
extern const void *List_Int16_AsReadOnly(List_Int16 * const);
extern void List_Int16_Append(Int16 *, List_Int16 * const);
extern void List_Int16_Delete(int, List_Int16 * const);
extern void List_Int16_Reverse(List_Int16 * const);
extern void List_Int16_Show(List_Int16 * const);
extern void List_Int16_Clear(List_Int16 * const);
extern int List_Int16_Count(List_Int16 * const);
extern int List_Int16_BinarySearch(Int16 *, List_Int16 * const);
extern char *List_Int16_GetType(List_Int16 * const);
extern Int16 **List_Int16_ToArray(List_Int16 * const);
extern const void *List_Int32_AsReadOnly(List_Int32 * const);
extern void List_Int32_Append(Int32 *, List_Int32 * const);
extern void List_Int32_Delete(int, List_Int32 * const);
extern void List_Int32_Reverse(List_Int32 * const);
extern void List_Int32_Show(List_Int32 * const);
extern void List_Int32_Clear(List_Int32 * const);
extern int List_Int32_Count(List_Int32 * const);
extern int List_Int32_BinarySearch(Int32 *, List_Int32 * const);
extern char *List_Int32_GetType(List_Int32 * const);
extern Int32 **List_Int32_ToArray(List_Int32 * const);
extern const void *List_Int64_AsReadOnly(List_Int64 * const);
extern void List_Int64_Append(Int64 *, List_Int64 * const);
extern void List_Int64_Delete(int, List_Int64 * const);
extern void List_Int64_Reverse(List_Int64 * const);
extern void List_Int64_Show(List_Int64 * const);
extern void List_Int64_Clear(List_Int64 * const);
extern int List_Int64_Count(List_Int64 * const);
extern int List_Int64_BinarySearch(Int64 *, List_Int64 * const);
extern char *List_Int64_GetType(List_Int64 * const);
extern Int64 **List_Int64_ToArray(List_Int64 * const);
extern const void *List_UInt16_AsReadOnly(List_UInt16 * const);
extern void List_UInt16_Append(UInt16 *, List_UInt16 * const);
extern void List_UInt16_Delete(int, List_UInt16 * const);
extern void List_UInt16_Reverse(List_UInt16 * const);
extern void List_UInt16_Show(List_UInt16 * const);
extern void List_UInt16_Clear(List_UInt16 * const);
extern int List_UInt16_Count(List_UInt16 * const);
extern int List_UInt16_BinarySearch(UInt16 *, List_UInt16 * const);
extern char *List_UInt16_GetType(List_UInt16 * const);
extern UInt16 **List_UInt16_ToArray(List_UInt16 * const);
extern const void *List_UInt32_AsReadOnly(List_UInt32 * const);
extern void List_UInt32_Append(UInt32 *, List_UInt32 * const);
extern void List_UInt32_Delete(int, List_UInt32 * const);
extern void List_UInt32_Reverse(List_UInt32 * const);
extern void List_UInt32_Show(List_UInt32 * const);
extern void List_UInt32_Clear(List_UInt32 * const);
extern int List_UInt32_Count(List_UInt32 * const);
extern int List_UInt32_BinarySearch(UInt32 *, List_UInt32 * const);
extern char *List_UInt32_GetType(List_UInt32 * const);
extern UInt32 **List_UInt32_ToArray(List_UInt32 * const);
extern const void *List_UInt64_AsReadOnly(List_UInt64 * const);
extern void List_UInt64_Append(UInt64 *, List_UInt64 * const);
extern void List_UInt64_Delete(int, List_UInt64 * const);
extern void List_UInt64_Reverse(List_UInt64 * const);
extern void List_UInt64_Show(List_UInt64 * const);
extern void List_UInt64_Clear(List_UInt64 * const);
extern int List_UInt64_Count(List_UInt64 * const);
extern int List_UInt64_BinarySearch(UInt64 *, List_UInt64 * const);
extern char *List_UInt64_GetType(List_UInt64 * const);
extern UInt64 **List_UInt64_ToArray(List_UInt64 * const);
extern const void *List_String_AsReadOnly(List_String * const);
extern void List_String_Append(String *, List_String * const);
extern void List_String_Delete(int, List_String * const);
extern void List_String_Reverse(List_String * const);
extern void List_String_Show(List_String * const);
extern void List_String_Clear(List_String * const);
extern int List_String_Count(List_String * const);
extern int List_String_BinarySearch(String *, List_String * const);
extern char *List_String_GetType(List_String * const);
extern String **List_String_ToArray(List_String * const);
extern char *ReadOnlyList_Int16_GetType(ReadOnlyList_Int16 * const);
extern int ReadOnlyList_Int16_IndexOf(Int16 *, ReadOnlyList_Int16 * const);
extern int ReadOnlyList_Int16_Count(ReadOnlyList_Int16 * const);
extern uint8_t ReadOnlyList_Int16_Contains(Int16 *, ReadOnlyList_Int16 * const);
extern uint8_t ReadOnlyList_Int16_Equals(void *, ReadOnlyList_Int16 * const);
extern void ReadOnlyList_Int16_CopyTo(Int16 **, int, ReadOnlyList_Int16 * const);
extern char *ReadOnlyList_Int32_GetType(ReadOnlyList_Int32 * const);
extern int ReadOnlyList_Int32_IndexOf(Int32 *, ReadOnlyList_Int32 * const);
extern int ReadOnlyList_Int32_Count(ReadOnlyList_Int32 * const);
extern uint8_t ReadOnlyList_Int32_Contains(Int32 *, ReadOnlyList_Int32 * const);
extern uint8_t ReadOnlyList_Int32_Equals(void *, ReadOnlyList_Int32 * const);
extern void ReadOnlyList_Int32_CopyTo(Int32 **, int, ReadOnlyList_Int32 * const);
extern char *ReadOnlyList_Int64_GetType(ReadOnlyList_Int64 * const);
extern int ReadOnlyList_Int64_IndexOf(Int64 *, ReadOnlyList_Int64 * const);
extern int ReadOnlyList_Int64_Count(ReadOnlyList_Int64 * const);
extern uint8_t ReadOnlyList_Int64_Contains(Int64 *, ReadOnlyList_Int64 * const);
extern uint8_t ReadOnlyList_Int64_Equals(void *, ReadOnlyList_Int64 * const);
extern void ReadOnlyList_Int64_CopyTo(Int64 **, int, ReadOnlyList_Int64 * const);
extern char *ReadOnlyList_UInt16_GetType(ReadOnlyList_UInt16 * const);
extern int ReadOnlyList_UInt16_IndexOf(UInt16 *, ReadOnlyList_UInt16 * const);
extern int ReadOnlyList_UInt16_Count(ReadOnlyList_UInt16 * const);
extern uint8_t ReadOnlyList_UInt16_Contains(UInt16 *, ReadOnlyList_UInt16 * const);
extern uint8_t ReadOnlyList_UInt16_Equals(void *, ReadOnlyList_UInt16 * const);
extern void ReadOnlyList_UInt16_CopyTo(UInt16 **, int, ReadOnlyList_UInt16 * const);
extern char *ReadOnlyList_UInt32_GetType(ReadOnlyList_UInt32 * const);
extern int ReadOnlyList_UInt32_IndexOf(UInt32 *, ReadOnlyList_UInt32 * const);
extern int ReadOnlyList_UInt32_Count(ReadOnlyList_UInt32 * const);
extern uint8_t ReadOnlyList_UInt32_Contains(UInt32 *, ReadOnlyList_UInt32 * const);
extern uint8_t ReadOnlyList_UInt32_Equals(void *, ReadOnlyList_UInt32 * const);
extern void ReadOnlyList_UInt32_CopyTo(UInt32 **, int, ReadOnlyList_UInt32 * const);
extern char *ReadOnlyList_UInt64_GetType(ReadOnlyList_UInt64 * const);
extern int ReadOnlyList_UInt64_IndexOf(UInt64 *, ReadOnlyList_UInt64 * const);
extern int ReadOnlyList_UInt64_Count(ReadOnlyList_UInt64 * const);
extern uint8_t ReadOnlyList_UInt64_Contains(UInt64 *, ReadOnlyList_UInt64 * const);
extern uint8_t ReadOnlyList_UInt64_Equals(void *, ReadOnlyList_UInt64 * const);
extern void ReadOnlyList_UInt64_CopyTo(UInt64 **, int, ReadOnlyList_UInt64 * const);
extern char *ReadOnlyList_String_GetType(ReadOnlyList_String * const);
extern int ReadOnlyList_String_IndexOf(String *, ReadOnlyList_String * const);
extern int ReadOnlyList_String_Count(ReadOnlyList_String * const);
extern uint8_t ReadOnlyList_String_Contains(String *, ReadOnlyList_String * const);
extern uint8_t ReadOnlyList_String_Equals(void *, ReadOnlyList_String * const);
extern void ReadOnlyList_String_CopyTo(String **, int, ReadOnlyList_String * const);
extern void *ctorCheckArgs(const char *, int, ...);
typedef struct Stack_Int16Private Stack_Int16Private; typedef struct stack_Int16 { Object object; struct Stack_Int16Private *Stack_Int16Private;; void (*Push)(Int16 *); Int16 *(*Pop)(void); Int16 *(*PeekStackPointer)(void); } Stack_Int16;;
typedef struct Stack_Int32Private Stack_Int32Private; typedef struct stack_Int32 { Object object; struct Stack_Int32Private *Stack_Int32Private;; void (*Push)(Int32 *); Int32 *(*Pop)(void); Int32 *(*PeekStackPointer)(void); } Stack_Int32;;
typedef struct Stack_Int64Private Stack_Int64Private; typedef struct stack_Int64 { Object object; struct Stack_Int64Private *Stack_Int64Private;; void (*Push)(Int64 *); Int64 *(*Pop)(void); Int64 *(*PeekStackPointer)(void); } Stack_Int64;;
typedef struct Stack_UInt16Private Stack_UInt16Private; typedef struct stack_UInt16 { Object object; struct Stack_UInt16Private *Stack_UInt16Private;; void (*Push)(UInt16 *); UInt16 *(*Pop)(void); UInt16 *(*PeekStackPointer)(void); } Stack_UInt16;;
typedef struct Stack_UInt32Private Stack_UInt32Private; typedef struct stack_UInt32 { Object object; struct Stack_UInt32Private *Stack_UInt32Private;; void (*Push)(UInt32 *); UInt32 *(*Pop)(void); UInt32 *(*PeekStackPointer)(void); } Stack_UInt32;;
typedef struct Stack_UInt64Private Stack_UInt64Private; typedef struct stack_UInt64 { Object object; struct Stack_UInt64Private *Stack_UInt64Private;; void (*Push)(UInt64 *); UInt64 *(*Pop)(void); UInt64 *(*PeekStackPointer)(void); } Stack_UInt64;;
typedef struct Stack_StringPrivate Stack_StringPrivate; typedef struct stack_String { Object object; struct Stack_StringPrivate *Stack_StringPrivate;; void (*Push)(String *); String *(*Pop)(void); String *(*PeekStackPointer)(void); } Stack_String;;
extern void *NStack_Int16(void);
extern void *NStack_Int32(void);
extern void *NStack_Int64(void);
extern void *NStack_UInt16(void);
extern void *NStack_UInt32(void);
extern void *NStack_UInt64(void);
extern void *NStack_String(void);
extern void Stack_Int16_Push(Int16 *, Stack_Int16 * const);
extern Int16 *Stack_Int16_Pop(Stack_Int16 * const);
extern Int16 *Stack_Int16_PeekStackPointer(Stack_Int16 * const);
extern void Stack_Int32_Push(Int32 *, Stack_Int32 * const);
extern Int32 *Stack_Int32_Pop(Stack_Int32 * const);
extern Int32 *Stack_Int32_PeekStackPointer(Stack_Int32 * const);
extern void Stack_Int64_Push(Int64 *, Stack_Int64 * const);
extern Int64 *Stack_Int64_Pop(Stack_Int64 * const);
extern Int64 *Stack_Int64_PeekStackPointer(Stack_Int64 * const);
extern void Stack_UInt16_Push(UInt16 *, Stack_UInt16 * const);
extern UInt16 *Stack_UInt16_Pop(Stack_UInt16 * const);
extern UInt16 *Stack_UInt16_PeekStackPointer(Stack_UInt16 * const);
extern void Stack_UInt32_Push(UInt32 *, Stack_UInt32 * const);
extern UInt32 *Stack_UInt32_Pop(Stack_UInt32 * const);
extern UInt32 *Stack_UInt32_PeekStackPointer(Stack_UInt32 * const);
extern void Stack_UInt64_Push(UInt64 *, Stack_UInt64 * const);
extern UInt64 *Stack_UInt64_Pop(Stack_UInt64 * const);
extern UInt64 *Stack_UInt64_PeekStackPointer(Stack_UInt64 * const);
extern void Stack_String_Push(String *, Stack_String * const);
extern String *Stack_String_Pop(Stack_String * const);
extern String *Stack_String_PeekStackPointer(Stack_String * const);
typedef struct Array_Int16Private Array_Int16Private; typedef struct array_Int16 { Object object; struct Array_Int16Private *Array_Int16Private;; Int16 *(*Get)(int); struct array_Int16 *(*Empty)(void); char *(*GetType)(void); uint8_t (*Equals)(void *); Int16 *(*IndexOf)(struct array_Int16 *, const Int16 *); void (*Resize)(Int32 *); void (*Reverse)(void); void (*SetValue)(Int16 *, Int32 *); String *(*ToString)(void); const int (*Length)(void); } Array_Int16;;
typedef struct Array_Int32Private Array_Int32Private; typedef struct array_Int32 { Object object; struct Array_Int32Private *Array_Int32Private;; Int32 *(*Get)(int); struct array_Int32 *(*Empty)(void); char *(*GetType)(void); uint8_t (*Equals)(void *); Int32 *(*IndexOf)(struct array_Int32 *, const Int32 *); void (*Resize)(Int32 *); void (*Reverse)(void); void (*SetValue)(Int32 *, Int32 *); String *(*ToString)(void); const int (*Length)(void); } Array_Int32;;
typedef struct Array_Int64Private Array_Int64Private; typedef struct array_Int64 { Object object; struct Array_Int64Private *Array_Int64Private;; Int64 *(*Get)(int); struct array_Int64 *(*Empty)(void); char *(*GetType)(void); uint8_t (*Equals)(void *); Int64 *(*IndexOf)(struct array_Int64 *, const Int64 *); void (*Resize)(Int32 *); void (*Reverse)(void); void (*SetValue)(Int64 *, Int32 *); String *(*ToString)(void); const int (*Length)(void); } Array_Int64;;
typedef struct Array_UInt16Private Array_UInt16Private; typedef struct array_UInt16 { Object object; struct Array_UInt16Private *Array_UInt16Private;; UInt16 *(*Get)(int); struct array_UInt16 *(*Empty)(void); char *(*GetType)(void); uint8_t (*Equals)(void *); UInt16 *(*IndexOf)(struct array_UInt16 *, const UInt16 *); void (*Resize)(Int32 *); void (*Reverse)(void); void (*SetValue)(UInt16 *, Int32 *); String *(*ToString)(void); const int (*Length)(void); } Array_UInt16;;
typedef struct Array_UInt32Private Array_UInt32Private; typedef struct array_UInt32 { Object object; struct Array_UInt32Private *Array_UInt32Private;; UInt32 *(*Get)(int); struct array_UInt32 *(*Empty)(void); char *(*GetType)(void); uint8_t (*Equals)(void *); UInt32 *(*IndexOf)(struct array_UInt32 *, const UInt32 *); void (*Resize)(Int32 *); void (*Reverse)(void); void (*SetValue)(UInt32 *, Int32 *); String *(*ToString)(void); const int (*Length)(void); } Array_UInt32;;
typedef struct Array_UInt64Private Array_UInt64Private; typedef struct array_UInt64 { Object object; struct Array_UInt64Private *Array_UInt64Private;; UInt64 *(*Get)(int); struct array_UInt64 *(*Empty)(void); char *(*GetType)(void); uint8_t (*Equals)(void *); UInt64 *(*IndexOf)(struct array_UInt64 *, const UInt64 *); void (*Resize)(Int32 *); void (*Reverse)(void); void (*SetValue)(UInt64 *, Int32 *); String *(*ToString)(void); const int (*Length)(void); } Array_UInt64;;
typedef struct Array_StringPrivate Array_StringPrivate; typedef struct array_String { Object object; struct Array_StringPrivate *Array_StringPrivate;; String *(*Get)(int); struct array_String *(*Empty)(void); char *(*GetType)(void); uint8_t (*Equals)(void *); String *(*IndexOf)(struct array_String *, const String *); void (*Resize)(Int32 *); void (*Reverse)(void); void (*SetValue)(String *, Int32 *); String *(*ToString)(void); const int (*Length)(void); } Array_String;;
extern void *NArray_Int16(size_t);
extern void *NArray_Int32(size_t);
extern void *NArray_Int64(size_t);
extern void *NArray_UInt16(size_t);
extern void *NArray_UInt32(size_t);
extern void *NArray_UInt64(size_t);
extern void *NArray_String(size_t);
extern Int16 *Array_Int16_Get(int, Array_Int16 * const);
extern Array_Int16 *Array_Int16_Empty(Array_Int16 * const);
extern char *Array_Int16_GetType(Array_Int16 * const);
extern uint8_t Array_Int16_Equals(void *, Array_Int16 * const);
extern Int16 *Array_Int16_IndexOf(const Int16 *, Array_Int16 * const);
extern void Array_Int16_Resize(Int32 *, Array_Int16 * const);
extern void Array_Int16_Reverse(Array_Int16 * const);
extern void Array_Int16_SetValue(Int16 *, Int32 *, Array_Int16 * const);
extern String *Array_Int16_ToString(Array_Int16 * const);
extern const int Array_Int16_Length(Array_Int16 * const);
extern Int32 *Array_Int32_Get(int, Array_Int32 * const);
extern Array_Int32 *Array_Int32_Empty(Array_Int32 * const);
extern char *Array_Int32_GetType(Array_Int32 * const);
extern uint8_t Array_Int32_Equals(void *, Array_Int32 * const);
extern Int32 *Array_Int32_IndexOf(const Int32 *, Array_Int32 * const);
extern void Array_Int32_Resize(Int32 *, Array_Int32 * const);
extern void Array_Int32_Reverse(Array_Int32 * const);
extern void Array_Int32_SetValue(Int32 *, Int32 *, Array_Int32 * const);
extern String *Array_Int32_ToString(Array_Int32 * const);
extern const int Array_Int32_Length(Array_Int32 * const);
extern Int64 *Array_Int64_Get(int, Array_Int64 * const);
extern Array_Int64 *Array_Int64_Empty(Array_Int64 * const);
extern char *Array_Int64_GetType(Array_Int64 * const);
extern uint8_t Array_Int64_Equals(void *, Array_Int64 * const);
extern Int64 *Array_Int64_IndexOf(const Int64 *, Array_Int64 * const);
extern void Array_Int64_Resize(Int32 *, Array_Int64 * const);
extern void Array_Int64_Reverse(Array_Int64 * const);
extern void Array_Int64_SetValue(Int64 *, Int32 *, Array_Int64 * const);
extern String *Array_Int64_ToString(Array_Int64 * const);
extern const int Array_Int64_Length(Array_Int64 * const);
extern UInt16 *Array_UInt16_Get(int, Array_UInt16 * const);
extern Array_UInt16 *Array_UInt16_Empty(Array_UInt16 * const);
extern char *Array_UInt16_GetType(Array_UInt16 * const);
extern uint8_t Array_UInt16_Equals(void *, Array_UInt16 * const);
extern UInt16 *Array_UInt16_IndexOf(const UInt16 *, Array_UInt16 * const);
extern void Array_UInt16_Resize(Int32 *, Array_UInt16 * const);
extern void Array_UInt16_Reverse(Array_UInt16 * const);
extern void Array_UInt16_SetValue(UInt16 *, Int32 *, Array_UInt16 * const);
extern String *Array_UInt16_ToString(Array_UInt16 * const);
extern const int Array_UInt16_Length(Array_UInt16 * const);
extern UInt32 *Array_UInt32_Get(int, Array_UInt32 * const);
extern Array_UInt32 *Array_UInt32_Empty(Array_UInt32 * const);
extern char *Array_UInt32_GetType(Array_UInt32 * const);
extern uint8_t Array_UInt32_Equals(void *, Array_UInt32 * const);
extern UInt32 *Array_UInt32_IndexOf(const UInt32 *, Array_UInt32 * const);
extern void Array_UInt32_Resize(Int32 *, Array_UInt32 * const);
extern void Array_UInt32_Reverse(Array_UInt32 * const);
extern void Array_UInt32_SetValue(UInt32 *, Int32 *, Array_UInt32 * const);
extern String *Array_UInt32_ToString(Array_UInt32 * const);
extern const int Array_UInt32_Length(Array_UInt32 * const);
extern UInt64 *Array_UInt64_Get(int, Array_UInt64 * const);
extern Array_UInt64 *Array_UInt64_Empty(Array_UInt64 * const);
extern char *Array_UInt64_GetType(Array_UInt64 * const);
extern uint8_t Array_UInt64_Equals(void *, Array_UInt64 * const);
extern UInt64 *Array_UInt64_IndexOf(const UInt64 *, Array_UInt64 * const);
extern void Array_UInt64_Resize(Int32 *, Array_UInt64 * const);
extern void Array_UInt64_Reverse(Array_UInt64 * const);
extern void Array_UInt64_SetValue(UInt64 *, Int32 *, Array_UInt64 * const);
extern String *Array_UInt64_ToString(Array_UInt64 * const);
extern const int Array_UInt64_Length(Array_UInt64 * const);
extern String *Array_String_Get(int, Array_String * const);
extern Array_String *Array_String_Empty(Array_String * const);
extern char *Array_String_GetType(Array_String * const);
extern uint8_t Array_String_Equals(void *, Array_String * const);
extern String *Array_String_IndexOf(const String *, Array_String * const);
extern void Array_String_Resize(Int32 *, Array_String * const);
extern void Array_String_Reverse(Array_String * const);
extern void Array_String_SetValue(String *, Int32 *, Array_String * const);
extern String *Array_String_ToString(Array_String * const);
extern const int Array_String_Length(Array_String * const);
typedef struct SystemConvertPrivate SystemConvertPrivate;
typedef struct SystemConvert
{
    Object object;
    struct SystemConvertPrivate *SystemConvertPrivate;;
    void *(*Convert)(void);
    char *(*GetType)(void);
    struct
    {
        Char *(*ToChar_Internal)(void *, const char *);
        Char *(*ToChar_Class)(void *, ...);
        Char *(*ToChar_Integral)(void *, ...);
        Float *(*ToFloat_Internal)(void *, const char *);
        Float *(*ToFloat_Class)(void *, ...);
        Float *(*ToFloat_Integral)(void *, ...);
        Double *(*ToDouble_Internal)(void *, const char *);
        Double *(*ToDouble_Class)(void *, ...);
        Double *(*ToDouble_Integral)(void *, ...);
        Int16 *(*ToInt16_Internal)(void *, const char *);
        Int16 *(*ToInt16_Class)(void *, ...);
        Int16 *(*ToInt16_Integral)(void *, ...);
        Int32 *(*ToInt32_Internal)(void *, const char *);
        Int32 *(*ToInt32_Class)(void *, ...);
        Int32 *(*ToInt32_Integral)(void *, ...);
        Int64 *(*ToInt64_Internal)(void *, const char *);
        Int64 *(*ToInt64_Class)(void *, ...);
        Int64 *(*ToInt64_Integral)(void *, ...);
        UInt16 *(*ToUInt16_Internal)(void *, const char *);
        UInt16 *(*ToUInt16_Class)(void *, ...);
        UInt16 *(*ToUInt16_Integral)(void *, ...);
        UInt32 *(*ToUInt32_Internal)(void *, const char *);
        UInt32 *(*ToUInt32_Class)(void *, ...);
        UInt32 *(*ToUInt32_Integral)(void *, ...);
        UInt64 *(*ToUInt64_Internal)(void *, const char *);
        UInt64 *(*ToUInt64_Class)(void *, ...);
        UInt64 *(*ToUInt64_Integral)(void *, ...);
        String *(*ToString_Internal)(void *, const char *);
        String *(*ToString_Class)(void *, ...);
        String *(*ToString_Integral)(void *, ...);
    };
} SystemConvert;
SystemConvert *Convert;
extern void *SystemConvert_new(void);
extern char *SystemConvert_GetType(void);
extern Char *SystemConvert_ToChar_Internal(void *, const char *, SystemConvert * const);
extern Char *SystemConvert_ToChar_Class(void *, ...);
extern Char *SystemConvert_ToChar_Integral(void *, ...);
extern Float *SystemConvert_ToFloat_Internal(void *, const char *, SystemConvert * const);
extern Float *SystemConvert_ToFloat_Class(void *, ...);
extern Float *SystemConvert_ToFloat_Integral(void *, ...);
extern Double *SystemConvert_ToDouble_Internal(void *, const char *, SystemConvert * const);
extern Double *SystemConvert_ToDouble_Class(void *, ...);
extern Double *SystemConvert_ToDouble_Integral(void *, ...);
extern Int16 *SystemConvert_ToInt16_Internal(void *, const char *, SystemConvert * const);
extern Int16 *SystemConvert_ToInt16_Class(void *, ...);
extern Int16 *SystemConvert_ToInt16_Integral(void *, ...);
extern Int32 *SystemConvert_ToInt32_Internal(void *, const char *, SystemConvert * const);
extern Int32 *SystemConvert_ToInt32_Class(void *, ...);
extern Int32 *SystemConvert_ToInt32_Integral(void *, ...);
extern Int64 *SystemConvert_ToInt64_Internal(void *, const char *, SystemConvert * const);
extern Int64 *SystemConvert_ToInt64_Class(void *, ...);
extern Int64 *SystemConvert_ToInt64_Integral(void *, ...);
extern UInt16 *SystemConvert_ToUInt16_Internal(void *, const char *, SystemConvert * const);
extern UInt16 *SystemConvert_ToUInt16_Class(void *, ...);
extern UInt16 *SystemConvert_ToUInt16_Integral(void *, ...);
extern UInt32 *SystemConvert_ToUInt32_Internal(void *, const char *, SystemConvert * const);
extern UInt32 *SystemConvert_ToUInt32_Class(void *, ...);
extern UInt32 *SystemConvert_ToUInt32_Integral(void *, ...);
extern UInt64 *SystemConvert_ToUInt64_Internal(void *, const char *, SystemConvert * const);
extern UInt64 *SystemConvert_ToUInt64_Class(void *, ...);
extern UInt64 *SystemConvert_ToUInt64_Integral(void *, ...);
extern String *SystemConvert_ToString_Internal(void *, const char *, SystemConvert * const);
extern String *SystemConvert_ToString_Class(void *, ...);
extern String *SystemConvert_ToString_Integral(void *, ...);
typedef struct StackPtr
{
    void *__rPtr;
    void (*dtor)(void);
    struct StackPtr *next;
    char *__f;
} StackPtr;
typedef struct GarbageCollector
{
    Object object;
    size_t __maxHeapSize;
    void (*Collect)(void);
    void (*StackFinalizer)(void *, void (*)(void), const char *);
    void (*RegisterFinalizer)(void *, void (*)(void *, void *), void (*)(void));
    void (*Alloc)(size_t);
    void (*Free)(void *);
    size_t (*GetHeapSize)(void);
} GarbageCollector;
static void initGC(void);
extern void *GarbageCollector_GarbageCollector(void);
extern void GC_Collect(GarbageCollector * const);
extern void GC_StackFinalizer(void *, void (*)(void), const char *, GarbageCollector * const);
extern void GC_RegisterFinalizer(void *, void (*)(void *, void *), void (*)(void), GarbageCollector * const);
extern void *GC_Alloc(size_t, GarbageCollector * const);
extern void GC_Free(void *, GarbageCollector * const);
extern size_t GC_GetHeapSize(GarbageCollector * const);
extern void StandardDestructor(void *, void *);
extern char *stepOut(const char *);
struct GarbageCollector *GC;
static StackPtr *stackRoots = ((void *)0);
__attribute__((section(".preinit_array"), used)) static __typeof__(initGC) *ctorGC = &initGC;
static void initGC(void)
{
    GC = GarbageCollector_GarbageCollector();
    { ; ; ; ; ; ; ; GC_init(); ; ; ; };
    return;
}
typedef int __gwchar_t;

typedef struct
  {
    long int quot;
    long int rem;
  } imaxdiv_t;
extern intmax_t imaxabs (intmax_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
extern imaxdiv_t imaxdiv (intmax_t __numer, intmax_t __denom)
      __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__const__));
extern intmax_t strtoimax (const char *__restrict __nptr,
      char **__restrict __endptr, int __base) __attribute__ ((__nothrow__ , __leaf__));
extern uintmax_t strtoumax (const char *__restrict __nptr,
       char ** __restrict __endptr, int __base) __attribute__ ((__nothrow__ , __leaf__));
extern intmax_t wcstoimax (const __gwchar_t *__restrict __nptr,
      __gwchar_t **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__));
extern uintmax_t wcstoumax (const __gwchar_t *__restrict __nptr,
       __gwchar_t ** __restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__));


extern int __sigismember (const __sigset_t *, int);
extern int __sigaddset (__sigset_t *, int);
extern int __sigdelset (__sigset_t *, int);

typedef __sig_atomic_t sig_atomic_t;

typedef union sigval
  {
    int sival_int;
    void *sival_ptr;
  } sigval_t;
typedef __clock_t __sigchld_clock_t;
typedef struct
  {
    int si_signo;
    int si_errno;
    int si_code;
    union
      {
 int _pad[((128 / sizeof (int)) - 4)];
 struct
   {
     __pid_t si_pid;
     __uid_t si_uid;
   } _kill;
 struct
   {
     int si_tid;
     int si_overrun;
     sigval_t si_sigval;
   } _timer;
 struct
   {
     __pid_t si_pid;
     __uid_t si_uid;
     sigval_t si_sigval;
   } _rt;
 struct
   {
     __pid_t si_pid;
     __uid_t si_uid;
     int si_status;
     __sigchld_clock_t si_utime;
     __sigchld_clock_t si_stime;
   } _sigchld;
 struct
   {
     void *si_addr;
     short int si_addr_lsb;
   } _sigfault;
 struct
   {
     long int si_band;
     int si_fd;
   } _sigpoll;
 struct
   {
     void *_call_addr;
     int _syscall;
     unsigned int _arch;
   } _sigsys;
      } _sifields;
  } siginfo_t ;
enum
{
  SI_ASYNCNL = -60,
  SI_TKILL = -6,
  SI_SIGIO,
  SI_ASYNCIO,
  SI_MESGQ,
  SI_TIMER,
  SI_QUEUE,
  SI_USER,
  SI_KERNEL = 0x80
};
enum
{
  ILL_ILLOPC = 1,
  ILL_ILLOPN,
  ILL_ILLADR,
  ILL_ILLTRP,
  ILL_PRVOPC,
  ILL_PRVREG,
  ILL_COPROC,
  ILL_BADSTK
};
enum
{
  FPE_INTDIV = 1,
  FPE_INTOVF,
  FPE_FLTDIV,
  FPE_FLTOVF,
  FPE_FLTUND,
  FPE_FLTRES,
  FPE_FLTINV,
  FPE_FLTSUB
};
enum
{
  SEGV_MAPERR = 1,
  SEGV_ACCERR
};
enum
{
  BUS_ADRALN = 1,
  BUS_ADRERR,
  BUS_OBJERR,
  BUS_MCEERR_AR,
  BUS_MCEERR_AO
};
enum
{
  TRAP_BRKPT = 1,
  TRAP_TRACE
};
enum
{
  CLD_EXITED = 1,
  CLD_KILLED,
  CLD_DUMPED,
  CLD_TRAPPED,
  CLD_STOPPED,
  CLD_CONTINUED
};
enum
{
  POLL_IN = 1,
  POLL_OUT,
  POLL_MSG,
  POLL_ERR,
  POLL_PRI,
  POLL_HUP
};
typedef struct sigevent
  {
    sigval_t sigev_value;
    int sigev_signo;
    int sigev_notify;
    union
      {
 int _pad[((64 / sizeof (int)) - 4)];
 __pid_t _tid;
 struct
   {
     void (*_function) (sigval_t);
     pthread_attr_t *_attribute;
   } _sigev_thread;
      } _sigev_un;
  } sigevent_t;
enum
{
  SIGEV_SIGNAL = 0,
  SIGEV_NONE,
  SIGEV_THREAD,
  SIGEV_THREAD_ID = 4
};
typedef void (*__sighandler_t) (int);
extern __sighandler_t __sysv_signal (int __sig, __sighandler_t __handler)
     __attribute__ ((__nothrow__ , __leaf__));
extern __sighandler_t sysv_signal (int __sig, __sighandler_t __handler)
     __attribute__ ((__nothrow__ , __leaf__));

extern __sighandler_t signal (int __sig, __sighandler_t __handler)
     __attribute__ ((__nothrow__ , __leaf__));

extern int kill (__pid_t __pid, int __sig) __attribute__ ((__nothrow__ , __leaf__));
extern int killpg (__pid_t __pgrp, int __sig) __attribute__ ((__nothrow__ , __leaf__));

extern int raise (int __sig) __attribute__ ((__nothrow__ , __leaf__));

extern __sighandler_t ssignal (int __sig, __sighandler_t __handler)
     __attribute__ ((__nothrow__ , __leaf__));
extern int gsignal (int __sig) __attribute__ ((__nothrow__ , __leaf__));
extern void psignal (int __sig, const char *__s);
extern void psiginfo (const siginfo_t *__pinfo, const char *__s);
extern int __sigpause (int __sig_or_mask, int __is_sig);
extern int sigblock (int __mask) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__deprecated__));
extern int sigsetmask (int __mask) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__deprecated__));
extern int siggetmask (void) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__deprecated__));
typedef __sighandler_t sighandler_t;
typedef __sighandler_t sig_t;
extern int sigemptyset (sigset_t *__set) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int sigfillset (sigset_t *__set) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int sigaddset (sigset_t *__set, int __signo) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int sigdelset (sigset_t *__set, int __signo) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int sigismember (const sigset_t *__set, int __signo)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int sigisemptyset (const sigset_t *__set) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int sigandset (sigset_t *__set, const sigset_t *__left,
        const sigset_t *__right) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2, 3)));
extern int sigorset (sigset_t *__set, const sigset_t *__left,
       const sigset_t *__right) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2, 3)));
struct sigaction
  {
    union
      {
 __sighandler_t sa_handler;
 void (*sa_sigaction) (int, siginfo_t *, void *);
      }
    __sigaction_handler;
    __sigset_t sa_mask;
    int sa_flags;
    void (*sa_restorer) (void);
  };
extern int sigprocmask (int __how, const sigset_t *__restrict __set,
   sigset_t *__restrict __oset) __attribute__ ((__nothrow__ , __leaf__));
extern int sigsuspend (const sigset_t *__set) __attribute__ ((__nonnull__ (1)));
extern int sigaction (int __sig, const struct sigaction *__restrict __act,
        struct sigaction *__restrict __oact) __attribute__ ((__nothrow__ , __leaf__));
extern int sigpending (sigset_t *__set) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern int sigwait (const sigset_t *__restrict __set, int *__restrict __sig)
     __attribute__ ((__nonnull__ (1, 2)));
extern int sigwaitinfo (const sigset_t *__restrict __set,
   siginfo_t *__restrict __info) __attribute__ ((__nonnull__ (1)));
extern int sigtimedwait (const sigset_t *__restrict __set,
    siginfo_t *__restrict __info,
    const struct timespec *__restrict __timeout)
     __attribute__ ((__nonnull__ (1)));
extern int sigqueue (__pid_t __pid, int __sig, const union sigval __val)
     __attribute__ ((__nothrow__ , __leaf__));
extern const char *const _sys_siglist[65];
extern const char *const sys_siglist[65];
struct sigvec
  {
    __sighandler_t sv_handler;
    int sv_mask;
    int sv_flags;
  };
extern int sigvec (int __sig, const struct sigvec *__vec,
     struct sigvec *__ovec) __attribute__ ((__nothrow__ , __leaf__));
struct _fpx_sw_bytes
{
  __uint32_t magic1;
  __uint32_t extended_size;
  __uint64_t xstate_bv;
  __uint32_t xstate_size;
  __uint32_t padding[7];
};
struct _fpreg
{
  unsigned short significand[4];
  unsigned short exponent;
};
struct _fpxreg
{
  unsigned short significand[4];
  unsigned short exponent;
  unsigned short padding[3];
};
struct _xmmreg
{
  __uint32_t element[4];
};
struct _fpstate
{
  __uint16_t cwd;
  __uint16_t swd;
  __uint16_t ftw;
  __uint16_t fop;
  __uint64_t rip;
  __uint64_t rdp;
  __uint32_t mxcsr;
  __uint32_t mxcr_mask;
  struct _fpxreg _st[8];
  struct _xmmreg _xmm[16];
  __uint32_t padding[24];
};
struct sigcontext
{
  __uint64_t r8;
  __uint64_t r9;
  __uint64_t r10;
  __uint64_t r11;
  __uint64_t r12;
  __uint64_t r13;
  __uint64_t r14;
  __uint64_t r15;
  __uint64_t rdi;
  __uint64_t rsi;
  __uint64_t rbp;
  __uint64_t rbx;
  __uint64_t rdx;
  __uint64_t rax;
  __uint64_t rcx;
  __uint64_t rsp;
  __uint64_t rip;
  __uint64_t eflags;
  unsigned short cs;
  unsigned short gs;
  unsigned short fs;
  unsigned short __pad0;
  __uint64_t err;
  __uint64_t trapno;
  __uint64_t oldmask;
  __uint64_t cr2;
  __extension__ union
    {
      struct _fpstate * fpstate;
      __uint64_t __fpstate_word;
    };
  __uint64_t __reserved1 [8];
};
struct _xsave_hdr
{
  __uint64_t xstate_bv;
  __uint64_t reserved1[2];
  __uint64_t reserved2[5];
};
struct _ymmh_state
{
  __uint32_t ymmh_space[64];
};
struct _xstate
{
  struct _fpstate fpstate;
  struct _xsave_hdr xstate_hdr;
  struct _ymmh_state ymmh;
};
extern int sigreturn (struct sigcontext *__scp) __attribute__ ((__nothrow__ , __leaf__));
extern int siginterrupt (int __sig, int __interrupt) __attribute__ ((__nothrow__ , __leaf__));
struct sigstack
  {
    void *ss_sp;
    int ss_onstack;
  };
enum
{
  SS_ONSTACK = 1,
  SS_DISABLE
};
typedef struct sigaltstack
  {
    void *ss_sp;
    int ss_flags;
    size_t ss_size;
  } stack_t;
extern int sigstack (struct sigstack *__ss, struct sigstack *__oss)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__deprecated__));
extern int sigaltstack (const struct sigaltstack *__restrict __ss,
   struct sigaltstack *__restrict __oss) __attribute__ ((__nothrow__ , __leaf__));
extern int pthread_sigmask (int __how,
       const __sigset_t *__restrict __newmask,
       __sigset_t *__restrict __oldmask)__attribute__ ((__nothrow__ , __leaf__));
extern int pthread_kill (pthread_t __threadid, int __signo) __attribute__ ((__nothrow__ , __leaf__));
extern int pthread_sigqueue (pthread_t __threadid, int __signo,
        const union sigval __value) __attribute__ ((__nothrow__ , __leaf__));
extern int __libc_current_sigrtmin (void) __attribute__ ((__nothrow__ , __leaf__));
extern int __libc_current_sigrtmax (void) __attribute__ ((__nothrow__ , __leaf__));

__extension__ typedef long long int greg_t;
typedef greg_t gregset_t[23];
enum
{
  REG_R8 = 0,
  REG_R9,
  REG_R10,
  REG_R11,
  REG_R12,
  REG_R13,
  REG_R14,
  REG_R15,
  REG_RDI,
  REG_RSI,
  REG_RBP,
  REG_RBX,
  REG_RDX,
  REG_RAX,
  REG_RCX,
  REG_RSP,
  REG_RIP,
  REG_EFL,
  REG_CSGSFS,
  REG_ERR,
  REG_TRAPNO,
  REG_OLDMASK,
  REG_CR2
};
struct _libc_fpxreg
{
  unsigned short int significand[4];
  unsigned short int exponent;
  unsigned short int padding[3];
};
struct _libc_xmmreg
{
  __uint32_t element[4];
};
struct _libc_fpstate
{
  __uint16_t cwd;
  __uint16_t swd;
  __uint16_t ftw;
  __uint16_t fop;
  __uint64_t rip;
  __uint64_t rdp;
  __uint32_t mxcsr;
  __uint32_t mxcr_mask;
  struct _libc_fpxreg _st[8];
  struct _libc_xmmreg _xmm[16];
  __uint32_t padding[24];
};
typedef struct _libc_fpstate *fpregset_t;
typedef struct
  {
    gregset_t gregs;
    fpregset_t fpregs;
    __extension__ unsigned long long __reserved1 [8];
} mcontext_t;
typedef struct ucontext
  {
    unsigned long int uc_flags;
    struct ucontext *uc_link;
    stack_t uc_stack;
    mcontext_t uc_mcontext;
    __sigset_t uc_sigmask;
    struct _libc_fpstate __fpregs_mem;
  } ucontext_t;

extern int getcontext (ucontext_t *__ucp) __attribute__ ((__nothrow__));
extern int setcontext (const ucontext_t *__ucp) __attribute__ ((__nothrow__));
extern int swapcontext (ucontext_t *__restrict __oucp,
   const ucontext_t *__restrict __ucp) __attribute__ ((__nothrow__));
extern void makecontext (ucontext_t *__ucp, void (*__func) (void),
    int __argc, ...) __attribute__ ((__nothrow__ , __leaf__));

typedef uint64_t unw_word_t;
typedef int64_t unw_sword_t;
typedef long double unw_tdep_fpreg_t;
typedef enum
  {
    UNW_X86_64_RAX,
    UNW_X86_64_RDX,
    UNW_X86_64_RCX,
    UNW_X86_64_RBX,
    UNW_X86_64_RSI,
    UNW_X86_64_RDI,
    UNW_X86_64_RBP,
    UNW_X86_64_RSP,
    UNW_X86_64_R8,
    UNW_X86_64_R9,
    UNW_X86_64_R10,
    UNW_X86_64_R11,
    UNW_X86_64_R12,
    UNW_X86_64_R13,
    UNW_X86_64_R14,
    UNW_X86_64_R15,
    UNW_X86_64_RIP,
    UNW_TDEP_LAST_REG = UNW_X86_64_RIP,
    UNW_X86_64_CFA,
    UNW_TDEP_IP = UNW_X86_64_RIP,
    UNW_TDEP_SP = UNW_X86_64_RSP,
    UNW_TDEP_BP = UNW_X86_64_RBP,
    UNW_TDEP_EH = UNW_X86_64_RAX
  }
x86_64_regnum_t;
typedef struct unw_tdep_save_loc
  {
    char unused;
  }
unw_tdep_save_loc_t;
typedef ucontext_t unw_tdep_context_t;
typedef struct
  {
    char unused;
  }
unw_tdep_proc_info_t;
typedef enum
  {
    UNW_DYN_STOP = 0,
    UNW_DYN_SAVE_REG,
    UNW_DYN_SPILL_FP_REL,
    UNW_DYN_SPILL_SP_REL,
    UNW_DYN_ADD,
    UNW_DYN_POP_FRAMES,
    UNW_DYN_LABEL_STATE,
    UNW_DYN_COPY_STATE,
    UNW_DYN_ALIAS
  }
unw_dyn_operation_t;
typedef enum
  {
    UNW_INFO_FORMAT_DYNAMIC,
    UNW_INFO_FORMAT_TABLE,
    UNW_INFO_FORMAT_REMOTE_TABLE,
    UNW_INFO_FORMAT_ARM_EXIDX,
    UNW_INFO_FORMAT_IP_OFFSET,
  }
unw_dyn_info_format_t;
typedef struct unw_dyn_op
  {
    int8_t tag;
    int8_t qp;
    int16_t reg;
    int32_t when;
    unw_word_t val;
  }
unw_dyn_op_t;
typedef struct unw_dyn_region_info
  {
    struct unw_dyn_region_info *next;
    int32_t insn_count;
    uint32_t op_count;
    unw_dyn_op_t op[1];
  }
unw_dyn_region_info_t;
typedef struct unw_dyn_proc_info
  {
    unw_word_t name_ptr;
    unw_word_t handler;
    uint32_t flags;
    int32_t pad0;
    unw_dyn_region_info_t *regions;
  }
unw_dyn_proc_info_t;
typedef struct unw_dyn_table_info
  {
    unw_word_t name_ptr;
    unw_word_t segbase;
    unw_word_t table_len;
    unw_word_t *table_data;
  }
unw_dyn_table_info_t;
typedef struct unw_dyn_remote_table_info
  {
    unw_word_t name_ptr;
    unw_word_t segbase;
    unw_word_t table_len;
    unw_word_t table_data;
  }
unw_dyn_remote_table_info_t;
typedef struct unw_dyn_info
  {
    struct unw_dyn_info *next;
    struct unw_dyn_info *prev;
    unw_word_t start_ip;
    unw_word_t end_ip;
    unw_word_t gp;
    int32_t format;
    int32_t pad;
    union
      {
        unw_dyn_proc_info_t pi;
        unw_dyn_table_info_t ti;
        unw_dyn_remote_table_info_t rti;
      }
    u;
  }
unw_dyn_info_t;
typedef struct unw_dyn_info_list
  {
    uint32_t version;
    uint32_t generation;
    unw_dyn_info_t *first;
  }
unw_dyn_info_list_t;
extern void _U_dyn_register (unw_dyn_info_t *);
extern void _U_dyn_cancel (unw_dyn_info_t *);
typedef enum
  {
    UNW_ESUCCESS = 0,
    UNW_EUNSPEC,
    UNW_ENOMEM,
    UNW_EBADREG,
    UNW_EREADONLYREG,
    UNW_ESTOPUNWIND,
    UNW_EINVALIDIP,
    UNW_EBADFRAME,
    UNW_EINVAL,
    UNW_EBADVERSION,
    UNW_ENOINFO
  }
unw_error_t;
typedef enum
  {
    UNW_REG_IP = UNW_TDEP_IP,
    UNW_REG_SP = UNW_TDEP_SP,
    UNW_REG_EH = UNW_TDEP_EH,
    UNW_REG_LAST = UNW_TDEP_LAST_REG
  }
unw_frame_regnum_t;
typedef enum
  {
    UNW_CACHE_NONE,
    UNW_CACHE_GLOBAL,
    UNW_CACHE_PER_THREAD
  }
unw_caching_policy_t;
typedef int unw_regnum_t;
typedef struct unw_cursor
  {
    unw_word_t opaque[127];
  }
unw_cursor_t;
typedef unw_tdep_context_t unw_context_t;
typedef unw_tdep_fpreg_t unw_fpreg_t;
typedef struct unw_addr_space *unw_addr_space_t;
typedef struct unw_proc_info
  {
    unw_word_t start_ip;
    unw_word_t end_ip;
    unw_word_t lsda;
    unw_word_t handler;
    unw_word_t gp;
    unw_word_t flags;
    int format;
    int unwind_info_size;
    void *unwind_info;
    unw_tdep_proc_info_t extra;
  }
unw_proc_info_t;
typedef struct unw_accessors
  {
    int (*find_proc_info) (unw_addr_space_t, unw_word_t, unw_proc_info_t *,
      int, void *);
    void (*put_unwind_info) (unw_addr_space_t, unw_proc_info_t *, void *);
    int (*get_dyn_info_list_addr) (unw_addr_space_t, unw_word_t *, void *);
    int (*access_mem) (unw_addr_space_t, unw_word_t, unw_word_t *, int,
         void *);
    int (*access_reg) (unw_addr_space_t, unw_regnum_t, unw_word_t *, int,
         void *);
    int (*access_fpreg) (unw_addr_space_t, unw_regnum_t,
    unw_fpreg_t *, int, void *);
    int (*resume) (unw_addr_space_t, unw_cursor_t *, void *);
    int (*get_proc_name) (unw_addr_space_t, unw_word_t, char *, size_t,
     unw_word_t *, void *);
  }
unw_accessors_t;
typedef enum unw_save_loc_type
  {
    UNW_SLT_NONE,
    UNW_SLT_MEMORY,
    UNW_SLT_REG
  }
unw_save_loc_type_t;
typedef struct unw_save_loc
  {
    unw_save_loc_type_t type;
    union
      {
 unw_word_t addr;
 unw_regnum_t regnum;
      }
    u;
    unw_tdep_save_loc_t extra;
  }
unw_save_loc_t;
extern unw_addr_space_t _ULx86_64_create_addr_space (unw_accessors_t *, int);
extern void _ULx86_64_destroy_addr_space (unw_addr_space_t);
extern unw_accessors_t *_Ux86_64_get_accessors (unw_addr_space_t);
extern void _Ux86_64_flush_cache (unw_addr_space_t, unw_word_t, unw_word_t);
extern int _ULx86_64_set_caching_policy (unw_addr_space_t, unw_caching_policy_t);
extern const char *_Ux86_64_regname (unw_regnum_t);
extern int _ULx86_64_init_local (unw_cursor_t *, unw_context_t *);
extern int _ULx86_64_init_remote (unw_cursor_t *, unw_addr_space_t, void *);
extern int _ULx86_64_step (unw_cursor_t *);
extern int _ULx86_64_resume (unw_cursor_t *);
extern int _ULx86_64_get_proc_info (unw_cursor_t *, unw_proc_info_t *);
extern int _ULx86_64_get_proc_info_by_ip (unw_addr_space_t, unw_word_t,
        unw_proc_info_t *, void *);
extern int _ULx86_64_get_reg (unw_cursor_t *, int, unw_word_t *);
extern int _ULx86_64_set_reg (unw_cursor_t *, int, unw_word_t);
extern int _ULx86_64_get_fpreg (unw_cursor_t *, int, unw_fpreg_t *);
extern int _ULx86_64_set_fpreg (unw_cursor_t *, int, unw_fpreg_t);
extern int _ULx86_64_get_save_loc (unw_cursor_t *, int, unw_save_loc_t *);
extern int _ULx86_64_is_signal_frame (unw_cursor_t *);
extern int _ULx86_64_handle_signal_frame (unw_cursor_t *);
extern int _ULx86_64_get_proc_name (unw_cursor_t *, char *, size_t, unw_word_t *);
extern const char *_Ux86_64_strerror (int);
extern int unw_backtrace (void **, int);
extern unw_addr_space_t _ULx86_64_local_addr_space;
extern int _Ux86_64_getcontext (unw_tdep_context_t *);
extern int _Ux86_64_is_fpreg (int);

typedef long int __jmp_buf[8];
struct __jmp_buf_tag
  {
    __jmp_buf __jmpbuf;
    int __mask_was_saved;
    __sigset_t __saved_mask;
  };

typedef struct __jmp_buf_tag jmp_buf[1];
extern int setjmp (jmp_buf __env) __attribute__ ((__nothrow__));

extern int __sigsetjmp (struct __jmp_buf_tag __env[1], int __savemask) __attribute__ ((__nothrow__));
extern int _setjmp (struct __jmp_buf_tag __env[1]) __attribute__ ((__nothrow__));

extern void longjmp (struct __jmp_buf_tag __env[1], int __val)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__));

extern void _longjmp (struct __jmp_buf_tag __env[1], int __val)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__));
typedef struct __jmp_buf_tag sigjmp_buf[1];
extern void siglongjmp (sigjmp_buf __env, int __val)
     __attribute__ ((__nothrow__)) __attribute__ ((__noreturn__));

typedef struct Except_Reason
{
    Object object;
    char ExceptionMessage[70];
    char ExceptionType[50];
    char *file;
    int line;
    int (*StackTrace)(void);
    char *(*GetType)(void);
    uint8_t (*Equals)(const char *);
} ExceptionClass;
typedef struct Except_Frame
{
    struct Except_Frame *prev;
    jmp_buf env;
    const char *file;
    int line;
    const ExceptionClass *exception;
} ExceptionFrame;
typedef struct context_stack
{
 struct context_stack *prev;
 ucontext_t ctx_frame;
} ContextFrame;
typedef struct unwind_frame
{
 void *funcAddr;
 int statusFreed;
 void *addrPtr;
 void (*dtorPtr)(void);
} UnwindFrame;
enum
{
 ExceptionEntered = 0,
 ExceptionRaised,
 ExceptionHandled,
 ExceptionFinalized
};
enum unwindStatus
{
 UnwindSuccess = 0,
 UnwindMainReached,
 UnwindNoReg,
 UnwindNoInfo,
 UnwindNoSymbol
};
enum destructorStatus
{
 DestructorTopReached = 0,
 DestructorFramesAvailable,
 DestructorNull,
 DestructorFreeFailed,
 DestructorNoInfo,
};
typedef enum unwindStatus UnwindStatus;
typedef enum destructorStatus DestructorStatus;
typedef ExceptionClass Exception;
typedef ExceptionClass MemoryAccessException;
typedef ExceptionClass DivideByZeroException;
typedef ExceptionClass FileNotFoundException;
typedef ExceptionClass DataNotFoundException;
typedef ExceptionClass SystemInterruptedException;
typedef ExceptionClass SystemRuntimeException;
typedef ExceptionClass ArgumentException;
typedef ExceptionClass NullArgumentException;
typedef ExceptionClass FormatException;
typedef ExceptionClass InvalidCastException;
typedef ExceptionClass UnderflowException;
typedef ExceptionClass OverflowException;
extern void __enable_customized_exception(char *, char *, void *, void *(*)(void));
extern void isCustom(char *, char *);
extern void buildException(int, char *);
extern void *EnableException(const char *, int, int);
extern int ExceptionClass_stackTrace(ExceptionClass * const);
extern char *ExceptionClass_GetType(ExceptionClass * const);
extern uint8_t ExceptionClass_Equals(const char *, ExceptionClass * const);
extern void __except_init(struct sigaction *, stack_t *, ExceptionFrame *);
extern int __chk_except_name(void **, const char *, const char *);
extern int __chk_except_isBuiltin(const char *, const char *);
extern int __unwindPtrHandler(void);
extern int callBacktraceUnwind(const char *);
extern int __callLocalDestructors(const char *, const char *);
extern void *reflectStrFuncName(const char *);
extern void __exception_push_object(void *, const char *, void (*)(void));
extern void except_raise(const ExceptionClass *, const char *, int);
extern char *delete_char(char *, char *);
extern void dump_regs(void);
extern void handler(int, siginfo_t *, void *);
extern char exceptionName[256];
extern int exceptIndex;
extern ucontext_t __ctx, *__ctxPtr, *__context;
extern ucontext_t arrayCtx[10];
extern ExceptionFrame *stackException;
extern ContextFrame *stackContext;
extern UnwindFrame __unwindFrames[255];
extern int __countAllocInit;
Exception *exInternal;
void *__customEx;
typedef struct keyword
{
    Object object;
    int asciiNumber;
    char asciiKey;
    uint8_t (*Equals)(struct keyword *);
    char *(*GetType)(void);
} KeyInfo;
extern void *KeyInfo_new(void);
extern uint8_t KeyInfo_Equals(KeyInfo *, KeyInfo * const);
extern char *KeyInfo_GetType(KeyInfo * const);
typedef struct systemConsole
{
    Object object;
    int argc;
    char **argv;
    void (*ClearConsole)(void);
    Array_String *(*GetArgv)(void);
    Int32 *(*Read)(void);
    KeyInfo *(*ReadKey)(void);
    String *(*ReadLine)(void);
    void (*Write)(Char *);
    void (*WriteLine)(const char *, ...);
} SystemConsole;
static void StaticConsole(int, char **);
extern SystemConsole *SystemConsole_new(void);
extern void SystemConsole_ClearConsole(SystemConsole * const);
extern Array_String *SystemConsole_GetArgv(SystemConsole * const);
extern Int32 *SystemConsole_Read(SystemConsole * const);
extern KeyInfo *SystemConsole_ReadKey(SystemConsole * const);
extern String *SystemConsole_ReadLine(SystemConsole * const);
extern void SystemConsole_Write(Char *, SystemConsole * const);
extern void SystemConsole_WriteLine(const char *, ...);
extern void SystemConsole_VaWriteLine(const char *, va_list);
SystemConsole *Console;
__attribute__((section(".preinit_array"), used)) static __typeof__(StaticConsole) *ctorConsole = &StaticConsole;
static void StaticConsole(int argc, char **argv)
{
    Console = SystemConsole_new();
    Console->argc = argc;
    Console->argv = argv;
    return;
}
typedef struct A
{
    Object object;
    struct
    {
        int __vptrCount;
        int __vptrFlag;
    };
    vtable *vmtA;;
    void *(*A0)(void);
    void (*$A)(void);
    void (*virtualFunc)(void);
    void (*virtualFunc2)(void);
} A;
typedef struct B
{
    A;
    vtable *vmtB;;
    void *(*B0)(void);
    void (*$B)(void);
} B;
void *A_A0(A *);
void A_virtualFunc(int, A * const);
void A_virtualFunc2(A * const);
void A_$A(A * const);
void *B_B0(B * const);
void B_virtualFunc(int, B * const);
void B_virtualFunc2(B * const);
void B_$B(B * const);
int main(void)
{
    A *a = ({ A *this; if(!0) { this = ({ Object *this = Object_create(sizeof(A), 0); (void *)this; }); } else { this = ({ Object *this = Object_create(sizeof(A), 0); (void *)this; }); } do { this->A0 = Object_trampoline(&this->object, A_A0, 0); } while (0); Object_prepare(&this->object); this; });;
    B *b = ({ B *this; if(!0) { this = ({ Object *this = Object_create(sizeof(B), 0); (void *)this; }); } else { this = ({ Object *this = Object_create(sizeof(B), 0); (void *)this; }); } do { this->B0 = Object_trampoline(&this->object, B_B0, 0); } while (0); Object_prepare(&this->object); this; });;
    return 0;
}
__attribute__((optimize("O0"))) void *A_A0(A * const this)
{
    do { callprotect(&this->object); if(!strcmp(isPtrOnStack(this), "True")) { this->$A = Object_trampoline(&this->object, A_$A, 0); __exception_push_object(this, stepOut(__func__), this->$A); Object_prepare(&this->object); } else { this->$A = Object_trampoline(&this->object, A_$A, 0); GC->RegisterFinalizer(this, StandardDestructor, this->$A); Object_prepare(&this->object); } } while(0);
    this->__vptrCount = 0;
    this->__vptrFlag = 0;
    do { callprotect(&this->object); if(!this->__vptrFlag) __initialize_virtual_table(&this->vmtA, &this->__vptrFlag); if(this->__vptrFlag) { if(!this->vmtA) { buildException(0, ""); } else { do { this->virtualFunc = Object_trampoline(&this->object, A_virtualFunc, 1); } while (0); __register_virtual_method(this->vmtA, &this->__vptrCount, A_virtualFunc, this->virtualFunc, &this->object); } } } while(0);
    do { callprotect(&this->object); if(!this->__vptrFlag) __initialize_virtual_table(&this->vmtA, &this->__vptrFlag); if(this->__vptrFlag) { if(!this->vmtA) { buildException(0, ""); } else { do { this->virtualFunc2 = Object_trampoline(&this->object, A_virtualFunc2, 0); } while (0); __register_virtual_method(this->vmtA, &this->__vptrCount, A_virtualFunc2, this->virtualFunc2, &this->object); } } } while(0);
    return 0;
}
void A_virtualFunc(int a, A * const this)
{
    Console->WriteLine("En %s", __func__);
    return;
}
void A_virtualFunc2(A * const this)
{
    Console->WriteLine("En %s", __func__);
    return;
}
void A_$A(A * const this)
{
    Console->WriteLine("Destruyendo A");
    return;
}
void *B_B0(B *this)
{
    Console->WriteLine("Construyendo B");
    A *base = ({ A *this; if(!0) { this = ({ Object *this = Object_create(sizeof(A), 0); (void *)this; }); } else { this = ({ Object *this = Object_create(sizeof(A), 0); (void *)this; }); } do { this->A0 = Object_trampoline(&this->object, A_A0, 0); } while (0); Object_prepare(&this->object); this; });; base->A0();; do { if(!strcmp(isPtrOnStack(this), "True")) { ({ switch(0) { case 0: callprotect(&this->object); do { this->A0 = Object_trampoline(&this->object, A_token2(A, 0), 0); } while (0); Object_prepare(&this->object); this->A0();; break; case 1 ... 5 : callprotect(&this->object); do { this->A0 = Object_trampoline(&this->object, A_token2(A, 0), 0); } while (0); Object_prepare(&this->object); this->A0();; break; } }); } else { ({ int __sizeBase = sizeof(A); const char *str = "A"; char add[100] = "N"; switch(0) { case 0: isList("A") ? ({ strcat(add, str); this = getListBase(add); }) : ({ callprotect(&this->object); do { this->A0 = Object_trampoline(&this->object, A_A0, 0); } while(0); Object_prepare(&this->object); this->A0(); }); break; case 1 ... 5: isList("A") ? ({ buildException(5, ""); }) : ({ callprotect(&this->object); do { this->A0 = Object_trampoline(&this->object, A_A0, 0); } while(0); Object_prepare(&this->object); this->A0(); }); break; } callprotect(&this->object); }); } } while(0);
    this->vmtB = this->vmtA;
    do { callprotect(&this->object); if(!strcmp(isPtrOnStack(this), "True")) { this->$B = Object_trampoline(&this->object, B_$B, 0); __exception_push_object(this, stepOut(__func__), this->$B); Object_prepare(&this->object); } else { this->$B = Object_trampoline(&this->object, B_$B, 0); GC->RegisterFinalizer(this, StandardDestructor, this->$B); Object_prepare(&this->object); } } while(0);
    __register_overriden_method(this->vmtB, B_virtualFunc, &this->__vptrCount, "virtualFunc", 1, this->virtualFunc, &this->object);;
    __register_overriden_method(this->vmtB, B_virtualFunc2, &this->__vptrCount, "virtualFunc2", 0, this->virtualFunc2, &this->object);;
    base->virtualFunc();
    return 0;
}
void B_virtualFunc(int a, B * const this)
{
    Console->WriteLine("En %s", __func__);
}
void B_virtualFunc2(B * const this)
{
    Console->WriteLine("En %s", __func__);
}
void B_$B(B * const this)
{
    Console->WriteLine("Destruyendo B");
}
