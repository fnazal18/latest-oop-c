#include "prueba.h"

int main(void)
{
    Prueba *test = Prueba_ctor(1, 2, 3);
    PruebaDerivada *test2 = PruebaDerivada_ctor(4, 5, 6, 7);

    printf("Valor sin herencia: %d - %d - %d\n", test->x, test->y, test->GetPrivate(test));
    printf("Valor con herencia: %d - %d - %d - %d\n", test2->x, test2->y, test2->GetPrivate((Prueba *)test2), test2->GetPrivateDerivada(test2));
}