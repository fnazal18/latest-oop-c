#include "ObjectPrueba.h"


typedef struct CustomException
{
    /* Inherited */
    Exception;
    /* Mandatory */
    #define CustomException(x) __register_exception_class(#x, CustomException)
    #define REG_CustomException __register_exception_name(CustomException)
    /* Class for check its working */
    Int32 *numError;
    String *cualquierMensaje;
    /* Constructor/Destructor */
    void *(*CustomException0)(void);
    void (*$CustomException)(void);
} CustomException;

void *CustomException_CustomException0(CustomException * const);
void CustomException_$CustomException(CustomException * const);
void funcion(void);

optimize(0) int main(void)
{
    /**
     * Basic case. 
     * 
     * In this case-scenario the CustomException (which we defined) will be caught by catch statement
     * and safely will move through finally block and resume standard execution.
     * The use of Convert class is noticed here as well, marking down the fact works properly 
     * by manipulating an integral type which is expected to be turned onto a class and dereferencing it to
     * retrieve its value (In terms of properties, this method returns the real variable holder). 
     */
    try
    {
        Console->WriteLine("1. First Exception Handling");
        /* Call to trigger function */
        funcion();
    }
    /* Handling CustomException */
    catch(CustomException e)
    {
        Console->WriteLine(e->ExceptionMessage);
        Console->WriteLine(e->cualquierMensaje->Get());
        Console->WriteLine(e->numError->ToString()->Get());
        Console->WriteLine("%p", e->CustomException0);
    }
    finally
    {
        Console->WriteLine("Outside\n");
    }
    /**
     * Middle case.
     * 
     * Difference from the first case-scenario, here we are going to use the normal Exception 
     * as handler for the incoming throwing exception. 
     * As expected, we can't access variables scopped to CustomException class 'cause we dynamically 
     * cast away their variables by using only the standard Exception class. Therefore only 
     * ExceptionMessage and GetType() are provided in this example.
     */
    try
    {
        Console->WriteLine("2. Second Exception Handling");
        funcion();
    }
    catch(Exception e)
    {
        Console->WriteLine(e->ExceptionMessage);
        Console->WriteLine(e->GetType());
    }
    finally
    {
        Console->WriteLine("Outside\n");
    }
    /**
     * Last case.
     * 
     * When multiple exceptions may be thrown from an either function or method, 
     * the catch clause can be repeated based on different exceptions. In this case-scenario
     * we use multiple catch statements taking in consideration that function() may throw
     * either MemoryAccessException, FileNotOpenedException or CustomException. 
     * The exception handling will catch CustomException (which is thrown from function()) and 
     * handles it through finally clause finalizing its execution.
     */
    try
    {
        Console->WriteLine("3. Third Exception Handling");
        funcion();
    }
    /* In case of MemoryAccessException is triggered */
    catch(MemoryAccessException e)
    {
        Console->WriteLine(e->ExceptionMessage);
        Console->WriteLine(e->GetType());
    }
    /* Or FileNotOpeneException is triggered */
    catch(FileNotOpenedException e2)
    {
        Console->WriteLine(e2->ExceptionMessage);
        Console->WriteLine(e2->GetType());
    }
    /* The default and expected case is when CustomException is triggered */
    catch(CustomException e3)
    {
        Console->WriteLine(e3->ExceptionMessage);
        Console->WriteLine(e3->GetType());
        Console->WriteLine(e3->numError->ToString()->Get());
        Console->WriteLine(e3->cualquierMensaje->Get());
        Console->WriteLine("%p", e3->CustomException0);
    }
    finally
    {
        Console->WriteLine("Outside");
    }
    return 0;
}


/* Trigger function */
void funcion(void)
{
    throw new CustomException("Custom Exception was thrown");
}

/* Default constructor for CustomException class */
void *CustomException_CustomException0(CustomException * const this)
{
    Console->WriteLine("Constructing %s", this->GetType());
    this->numError = new Instance(Int32, 1);
    this->cualquierMensaje = new Instance(String, 1);
    this->numError->Int32(10);
    this->cualquierMensaje->String("Any message. Nothing quite important");
    Object_prepare(&this->object);
    return 0;
}

/* Destructor associated to class */
void CustomException_$CustomException(CustomException * const this)
{
    Console->WriteLine("Destroying class");
    return;
}